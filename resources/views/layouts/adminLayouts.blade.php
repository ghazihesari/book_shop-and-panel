<?php
use App\Http\Controller\Auth\AuthController;
?>
<!DOCTYPE html>
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<head>
    <meta charset="UTF-8" />
    <title>داشبورد </title>
     <meta content="width=device-width, initial-scale=1.0" name="viewport" />

     <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <!-- GLOBAL STYLES -->
    <link rel="stylesheet" href="<?= Url('assets/plugins/bootstrap/css/bootstrap.rtl.css'); ?>" />
    <link rel="stylesheet" href="<?= Url('assets/css/main.css'); ?>" />
    <link rel="stylesheet" href="<?= Url('assets/css/theme.css'); ?>" />
    <link rel="stylesheet" href="<?= Url('assets/css/MoneAdmin.css'); ?>" />
    <link rel="stylesheet" href="<?= Url('assets/plugins/Font-Awesome/css/font-awesome.css'); ?>" />

    <link href="<?= Url('assets/css/layout2.css'); ?>" rel="stylesheet" />
       <link href="<?= Url('assets/plugins/flot/examples/examples.css'); ?>" rel="stylesheet" />
       <link rel="stylesheet" href="<?= Url('assets/plugins/timeline/timeline.css'); ?>" />

       @yield('head')

</head>

<body class="padTop53 " >

    <!-- MAIN WRAPPER -->
    <div id="wrap" >
        

         <!-- HEADER SECTION -->
        <div id="top">

            <nav class="navbar navbar-inverse navbar-fixed-top " style="padding: 10px;">
                <a data-original-title="Show/Hide Menu" data-placement="bottom" data-tooltip="tooltip" class="accordion-toggle btn btn-primary btn-sm visible-xs" data-toggle="collapse" href="#menu" id="menu-toggle">
                    <i class="icon-align-justify"></i>
                </a>
                <!-- LOGO SECTION -->
                <header class="navbar-header">

                    <a href="index.html" class="navbar-brand">
                        <img src="<?= Url('assets/img/logo.png'); ?>" alt="" height="30" />
                        <h1 class="site-title">ایران نهاد</h1>
                    </a>
                </header>
                <!-- END LOGO SECTION -->
                <ul class="nav navbar-top-links navbar-left">

                    <!-- MESSAGES SECTION -->
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            @if( countmessage() == '0' )
                            @else
                            <span class="label label-success">{!! countmessage() !!}</span>  
                            @endif
                            <i class="icon-envelope-alt"></i>&nbsp; <i class="icon-chevron-down"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-messages">

                            @foreach( getMessages() as $message )
                            <li>
                                <a href="<?= Url('admin/ticket/ansewer/'.$message->id); ?>">
                                    <div>
                                       <strong>{{ $message->fname }}</strong>
                                        <span class="pull-left text-muted">
                                        </span>
                                    </div>
                                    <div>{!! countText( $message->content ) !!}
                                        <br />
                                        @if( $message->olaviyat == '1' )
                                        <span class="label label-primary"> اولویت زیاد</span> 
                                        @elseif( $message->olaviyat == '2' )
                                        <span class="label label-success">  اولویت متوسط</span>
                                        @elseif( $message->olaviyat == '3' )
                                        <span class="label label-danger">  اولویت کم</span>
                                        @endif
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            @endforeach
                            
                            <li>
                                <a class="text-center" href="<?= Url('admin/ticket'); ?>">
                                    <i class="icon-angle-left"></i>
                                    <strong>خواندن همه تیکت ها</strong>
                                </a>
                            </li>
                        </ul>

                    </li>
                    <!--END MESSAGES SECTION -->

                    <!--TASK SECTION -->

                    <!--END TASK SECTION -->

                    <!--ALERTS SECTION -->
                    <li class="chat-panel dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            @if( countcomment() == '0' )
                            @else
                            <span class="label label-info">{!! countcomment() !!}</span> 
                            @endif
                            <i class="icon-comments"></i>&nbsp; <i class="icon-chevron-down"></i>
                        </a>

                        <ul class="dropdown-menu dropdown-alerts">
                            @foreach( getComment() as $comment )
                            <li> 
                                <a href="<?= Url('admin/comments/'.$comment->id.'/edit'); ?>">
                                    <div>
                                    <span class="pull-left text-muted small"> {{ $comment->name_comments }} </span>
                                        <i class="icon-comment" ></i> {!! countText( $comment->content_comments ) !!}
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            @endforeach
                            <li>
                                <a class="text-center" href="<?= Url('admin/comments'); ?>">
                                    <i class="icon-angle-left"></i>
                                    <strong>مشاهده همه دیدگاه ها</strong>
                                </a>
                            </li>
                        </ul>

                    </li>
                    <!-- END ALERTS SECTION -->

                    <!--ADMIN SETTINGS SECTIONS -->

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user "></i>&nbsp; <i class="icon-chevron-down "></i>
                        </a>

                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="<?= Url('/logout'); ?>"><i class="icon-signout"></i> خروج </a>
                            </li>
                        </ul>

                    </li>
                    <!--END ADMIN SETTINGS -->
                </ul>

            </nav>

        </div>
        <!-- END HEADER SECTION -->



        <!-- MENU SECTION -->
       <div id="right">
            <div class="media user-media well-small">
                <a class="user-link" href="#">
                    <img class="media-object img-thumbnail user-img" alt="User Picture" src="<?= Url('assets/imageusers/'.Auth::user()->img); ?>" />
                </a>
                <br />
                <div class="media-body">
                    <h5 class="media-heading"> 
                    <?php
                        echo Auth::user()->fname;
                    ?>
                    </h5>
                    <ul class="list-unstyled user-info">
                       
                    </ul>
                </div>
                <br />
            </div>

            <ul id="menu" class="collapse">

                
                <li class="panel active">
                    <a href="<?= Url('admin/index'); ?>" >
                        <i class="icon-table"></i> داشبورد
	   
                       
                    </a>                   
                </li>


 

                <li class="panel ">
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav">
                        <i class="icon-tasks"> </i> مدیریت کتاب های سایت     
	   
                        <span class="pull-left">
                          <i class="icon-angle-right"></i>
                        </span>
                    </a>
                    <ul class="collapse" id="component-nav">
                       
                        <li class=""><a href="<?= Url('admin/books/create'); ?>"><i class="icon-angle-left"></i> ایجاد کتاب جدید </a></li>

                         <li class=""><a href="<?= Url('admin/books'); ?>"><i class="icon-angle-left"></i> نمایش و مدیریت کتب </a></li>
                        
                    </ul>
                </li>






                <li class="panel ">
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle collapsed" data-target="#form-nav">
                        <i class="icon-pencil"></i> مراکز پخش
                        
                        <span class="pull-left">
                            <i class="icon-angle-right"></i>
                        <!-- </span>
                          &nbsp; <span class="label label-info">6</span>&nbsp; -->

                    </a>
                    <ul class="collapse" id="form-nav">

                        <li class=""><a href="<?= Url('admin/pakhsh/create'); ?>"><i class="icon-angle-left"></i> ایجاد مرکز جدید </a></li>

                        <li class=""><a href="<?= Url('admin/pakhsh'); ?>"><i class="icon-angle-left"></i> نمایش و مدیریت مراکز </a></li>

                    </ul>
                </li>








                <li class="panel">
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#pagesr-nav">
                        <i class="icon-table"></i> مدیریت نویسندگان
	   
                        <span class="pull-left">
                            <i class="icon-angle-right"></i>
                        <!-- </span>
                          &nbsp; <span class="label label-info">6</span>&nbsp; -->
                    </a>
                    <ul class="collapse" id="pagesr-nav">
                        <li><a href="<?= Url('admin/moalef/create'); ?>"><i class="icon-angle-left"></i> ثبت نویسنده جدید </a></li>

                        <li><a href="<?= Url('admin/moalef'); ?>"><i class="icon-angle-left"></i>نمایش و مدیریت نویسندگان </a></li>
                        
                    </ul>
                </li>







                <li class="panel">
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#chart-nav">
                        <i class="icon-bar-chart"></i> مدیریت مترجمین
	   
                        <span class="pull-left">
                            <i class="icon-angle-right"></i>
                        </span>
                         
                    </a>
                    <ul class="collapse" id="chart-nav">

                        <li><a href="<?= Url('admin/motarjems/create'); ?>"><i class="icon-angle-left"></i> ثبت مترجم جدید </a></li>
                        <li><a href="<?= Url('admin/motarjems'); ?>"><i class="icon-angle-left"></i> نمایش و مدیریت مترجمین</a></li>
                        
                    </ul>
                </li>



                <li class="panel ">
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#component-nav2">
                        <i class="icon-tasks"> </i> مدیریت چاپ خانه ها    
       
                        <span class="pull-left">
                          <i class="icon-angle-right"></i>
                        </span>
                       
                    </a>
                    <ul class="collapse" id="component-nav2">
                       
                        <li class=""><a href="<?= Url('admin/chapkhoneh/create'); ?>"><i class="icon-angle-left"></i> ثبت چاپ خانه جدید </a></li>

                         <li class=""><a href="<?= Url('admin/chapkhoneh'); ?>"><i class="icon-angle-left"></i> نمایش چاپخانه ها </a></li>
                        
                    </ul>
                </li>





                <li class="panel ">
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle collapsed" data-target="#form-nav2">
                        <i class="icon-pencil"></i> مدیریت موضوعات کتب
       
                        <span class="pull-left">
                            <i class="icon-angle-right"></i>
                        </span>
                         
                    </a>
                    <ul class="collapse" id="form-nav2">

                        <li class=""><a href="<?= Url('admin/category/create'); ?>"><i class="icon-angle-left"></i> ثبت موضوع جدید </a></li>

                        <li class=""><a href="<?= Url('admin/category'); ?>"><i class="icon-angle-left"></i> نمایش موضوعات و مدیریت آن ها </a></li>

                    </ul>
                </li>








                <li class="panel">
                    <a href="#" data-parent="#menu" data-toggle="collapse" class="accordion-toggle" data-target="#pagesr-nav2">
                        <i class="icon-table"></i> مدیریت کاربران 
       
                        <span class="pull-left">
                            <i class="icon-angle-right"></i>
                        </span>
                    </a>
                    <ul class="collapse" id="pagesr-nav2">
                        <li><a href="<?= Url('admin/users/create'); ?>"><i class="icon-angle-left"></i> ثبت کاربر جدید </a></li>

                        <li><a href="<?= Url('admin/users'); ?>"><i class="icon-angle-left"></i>نمایش و مدیریت کاربران</a></li>
                        
                    </ul>
                </li>







                <li><a href="<?= Url('admin/sefareshs'); ?>"><i class="icon-table"></i> 
                    مدیریت سفارشات
                          &nbsp;
                        @if( countsefaresh() == 0 )
                        @else
                        <span class="label label-success">{{ countsefaresh() }}</span>&nbsp;
                        @endif
                </a></li>




                <li><a href="<?= Url('admin/comments'); ?>"><i class="icon-table"></i> دیدگاه ها 
                          &nbsp;
                        @if( countcomment() == 0 )
                        @else
                        <span class="label label-danger">{{ countcomment() }}</span>&nbsp; 
                        @endif
                </a></li>

 

                <li><a href="<?= Url('admin/ticket'); ?>"><i class="icon-table"></i> تیکت ها
                    &nbsp;
                    @if( countmessage() == 0 )
                    @else
                    <span class="label label-success">{!! countmessage() !!}</span>&nbsp; 
                    @endif
                </a></li> 




            </ul>

        </div>
        <!--END MENU SECTION -->





        <!--PAGE CONTENT -->
        <div id="content">
            <div class="inner" style="min-height: 700px;">
                @yield('content')

                    
            </div>

        </div>
        <!--END PAGE CONTENT -->





 
          <!-- RIGHT STRIP  SECTION -->
        <div id="left">

            <div class="well well-small" style="margin-top: 150px;">
                <a href="<?= Url('admin/ticket') ?>" class="btn btn-primary btn-block"> تیکت ها</a>
                <a href="<?= Url('admin/books') ?>" class="btn btn-info btn-block"> کتاب ها </a>
                <a href="<?= Url('admin/users') ?>" class="btn btn-success btn-block"> کاربران </a>
                <a href="<?= Url('admin/category') ?>" class="btn btn-danger btn-block"> موضوعات </a>
                <a href="<?= Url('admin/comments') ?>" class="btn btn-warning btn-block"> دیدگاه ها </a>
                <a href="<?= Url('admin/sefareshs') ?>" class="btn btn-primary btn-block"> سفارشات </a>
            </div>


        </div>
         <!-- END RIGHT STRIP  SECTION -->
    </div>

    <!--END MAIN WRAPPER -->

    <!-- FOOTER -->
    <div id="footer">
        <p>کلیه حقوق سایت متعلق به <a href="<?= Url('/'); ?>">فروشگاه کتاب </a> می باشد.</p>
    </div>
    <!--END FOOTER -->


    <!-- GLOBAL SCRIPTS -->
    <script src="<?= Url('assets/plugins/jquery-2.0.3.min.js'); ?>"></script>
     <script src="<?= Url('assets/plugins/bootstrap/js/bootstrap.rtl.js'); ?>"></script>
    <script src="<?= Url('assets/plugins/modernizr-2.6.2-respond-1.1.0.min.js'); ?>"></script>
    <!-- END GLOBAL SCRIPTS -->

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= Url('assets/plugins/flot/jquery.flot.js'); ?>"></script>
    <script src="<?= Url('assets/plugins/flot/jquery.flot.resize.js'); ?>"></script>
    <script src="<?= Url('assets/plugins/flot/jquery.flot.time.js'); ?>"></script>
     <script  src="<?= Url('assets/plugins/flot/jquery.flot.stack.js'); ?>"></script>
    <script src="<?= Url('assets/js/for_index.js'); ?>"></script>
   
   @yield('script')
    <!-- END PAGE LEVEL SCRIPTS -->


</body>

    <!-- END BODY -->
</html>

<?php
 
use App\CommentModel;
use App\SefareshModel;
use App\CallMeModel;

function countcomment()
{
    $count = CommentModel::where('state','0')->count();
    return $count;
}

function getComment()
{
    $comment = CommentModel::where('state','0')->take(10)->get();
    return $comment;
}


function countsefaresh(){
    $count = SefareshModel::where('state',1)->count();
    return $count;
}

function countmessage(){
    $count = CallMeModel::where('state',0)->count();
    return $count;
}


function getMessages()
{
    $message = CallMeModel::where('state','0')->take(5)->get();
    return $message;
}


function countText( $text )
{
    $gettext = substr($text, 0, 60);
    return $gettext;
}

?>

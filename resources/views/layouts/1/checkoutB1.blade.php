 
<!DOCTYPE html>
<!--[if lt IE 8]>      <html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <title>بررسی و ثبت سفارش خرید</title>

    
    <!-- Twitter Bootstrap -->
    <link href="<?= Url('themes/1/stylesheets/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?= Url('themes/1/stylesheets/responsive.css'); ?>" rel="stylesheet">
    <!-- Slider Revolution -->
    <link rel="stylesheet" href="<?= Url('themes/1/js/rs-plugin/css/settings.css'); ?>" type="text/css"/>
    <!-- jQuery UI -->
    <link rel="stylesheet" href="<?= Url('themes/1/js/jquery-ui-1.10.3/css/smoothness/jquery-ui-1.10.3.custom.min.css'); ?>" type="text/css"/>
    <!-- PrettyPhoto -->
    <link rel="stylesheet" href="<?= Url('themes/1/js/prettyphoto/css/prettyPhoto.css'); ?>" type="text/css"/>
    <!-- main styles -->
     
    <link href="<?= Url('themes/1/stylesheets/main.css'); ?>" rel="stylesheet">
     
    

    <!-- Modernizr -->
    <script src="<?= Url('themes/1/js/modernizr.custom.56918.js'); ?>"></script>    

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?= Url('themes/1/images/apple-touch/144.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?= Url('themes/1/images/apple-touch/114.png'); ?>">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?= Url('themes/1/images/apple-touch/72.png'); ?>">
    <link rel="apple-touch-icon-precomposed" href="<?= Url('themes/1/images/apple-touch/57.png'); ?>">
    <link rel="shortcut icon" href="<?= Url('themes/1/images/apple-touch/57.png'); ?>">
  </head>

   
  <body class=" checkout-page">
    
    <div class="master-wrapper">
     
    
    
    <div class="container">
        <div class="row">
            
            <!--  ==========  -->
            <!--  = Main content =  -->
            <!--  ==========  -->
            <section class="span12">
                
                <div class="checkout-container">
                    <div class="row">
                    	<div class="span10 offset1">
                    	    
                    	    

                            @yield('content')

							
							
                    	</div>
                    </div>
                </div>
                
                
            </section> <!-- /main content -->
        
        </div>
    </div> <!-- /container -->
    
     
    
     
    </div> <!-- end of master-wrapper -->
    


    <!--  ==========  -->
    <!--  = JavaScript =  -->
    <!--  ==========  -->
    
    
    
    <!--  = jQuery - CDN with local fallback =  -->
    
    <!--  = _ =  -->
    <script src="<?= Url('themes/1/js/underscore/underscore-min.js'); ?>" type="text/javascript"></script>
    
    <!--  = Bootstrap =  -->
    <script src="<?= Url('themes/1/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = Slider Revolution =  -->
    <script src="<?= Url('themes/1/js/rs-plugin/pluginsources/jquery.themepunch.plugins.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= Url('themes/1/js/rs-plugin/js/jquery.themepunch.revolution.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = CarouFredSel =  -->
    <script src="<?= Url('themes/1/js/jquery.carouFredSel-6.2.1-packed.js'); ?>" type="text/javascript"></script>
    
    <!--  = jQuery UI =  -->
    <script src="<?= Url('themes/1/js/jquery-ui-1.10.3/js/jquery-ui-1.10.3.custom.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= Url('themes/1/js/jquery-ui-1.10.3/touch-fix.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = Isotope =  -->
    <script src="<?= Url('themes/1/js/isotope/jquery.isotope.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = Tour =  -->
    <script src="<?= Url('themes/1/js/bootstrap-tour/build/js/bootstrap-tour.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = PrettyPhoto =  -->
    <script src="<?= Url('themes/1/js/prettyphoto/js/jquery.prettyPhoto.js'); ?>" type="text/javascript"></script>
    
    <!--  = Google Maps API =  -->
    <script type="text/javascript" src="<?= Url('themes/1/js/goMap/js/jquery.gomap-1.3.2.min.js'); ?>"></script>
    
    <!--  = Custom JS =  -->
    <script src="<?= Url('themes/1/js/custom.js'); ?>" type="text/javascript"></script>

  </body>
</html>

    
    
 
    @yield('headers')

     

     <!--  ==========  -->
    <!--  = Slider Revolution =  -->
    <!--  ==========  -->
    <div class="fullwidthbanner-container">
        <div class="fullwidthbanner">
            <ul>
                
                <li data-transition="premium-random" data-slotamount="3">
                    <img src="<?= Url('themes/1/images/dummy/slides/2/slide.jpg'); ?>" alt="slider img" width="1400" height="377" />
                    
                    <!-- woman -->
                    <div class="caption lfb ltb"
                        data-x="800" 
                        data-y="50"
                        data-speed="1000" 
                        data-start="1000" 
                        data-easing="easeInOutCubic">
                        <img src="<?= Url('themes/1/images/dummy/slides/2/woman.png'); ?>" alt="woman" width="361" height="374" />
                    </div>
                    
                    <!-- plane -->
                    <div class="caption lfl str"
                        data-x="400" 
                        data-y="20"
                        data-speed="10000" 
                        data-start="1000" 
                        data-easing="linear">
                        <img src="<?= Url('themes/1/images/dummy/slides/2/plane.png'); ?>" alt="aircraft" width="117" height="28" />
                    </div>
                    
                    <!-- texts -->
                    <div class="caption lfl big_theme"
                        data-x="120" 
                        data-y="120"
                        data-speed="1000" 
                        data-start="500" 
                        data-easing="easeInOutBack">
                        کتاب یار مهربان است
                    </div>
                    
                    <div class="caption lfl small_theme"
                        data-x="120" 
                        data-y="190"
                        data-speed="1000" 
                        data-start="700" 
                        data-easing="easeInOutBack">
                        کتاب ها را تنها نزارید
                    </div>
                    
                </li><!-- /slide -->

                <li data-transition="premium-random" data-slotamount="3">
                    <img src="<?= Url('themes/1/images/dummy/slides/1/slide.jpg'); ?>" alt="slider img" width="1400" height="377" />
            
                </li><!-- /slide -->
                
                <li data-transition="premium-random" data-slotamount="3">
                    <img src="<?= Url('themes/1/images/dummy/slides/3/slide.jpg'); ?>" alt="slider img" width="1400" height="377" />
                    
                </li><!-- /slide -->
                
                <li data-transition="premium-random" data-slotamount="3">
                    <img src="<?= Url('themes/1/images/dummy/slides/4/slide.jpg'); ?>" alt="slider img" width="1400" height="377" />
                    
                </li><!-- /slide -->
            </ul>
            <div class="tp-bannertimer"></div>
        </div>
        <!--  ==========  -->
        <!--  = Nav Arrows =  -->
        <!--  ==========  -->
        <div id="sliderRevLeft"><i class="icon-chevron-left"></i></div>
        <div id="sliderRevRight"><i class="icon-chevron-right"></i></div>
    </div> <!-- /slider revolution -->


    <!--  ==========  -->
    <!--  = Main container =  -->
    <!--  ==========  -->
    <div class="container">
        <div class="row">
            <div class="span12">
                <div class="push-up over-slider blocks-spacer">
                    
                    <!--  ==========  -->
                    <!--  = Three Banners =  -->
                    <!--  ==========  -->
                    <div class="row">
                        <div class="span4">
                            <a href="#" class="btn btn-block banner">
                                <span class="title"><span class="light">فروش</span> زمستانی</span>
                                <em>تا 60% تخفیف روی کتاب ها</em>
                            </a>
                        </div>
                        <div class="span4">
                            <a href="#" class="btn btn-block colored banner">
                                <span class="title"><span class="light">ارسال</span> رایگان</span>
                                <em>برای خرید های بیش از 69000 تومان</em>
                            </a>
                        </div>
                        <div class="span4">
                            <a href="<?= Url('/shop') ?>" class="btn btn-block banner">
                                <span class="title"><span class="light">کتاب های</span> جدید</span>
                                <em>از فروشگاه ما دیدن کنید</em>
                            </a>
                        </div>
                    </div> <!-- /three banners -->
                </div>
            </div>
        </div>
        
        <!--  ==========  -->
        <!--  = Featured Items =  -->
        <!--  ==========  -->
        <div class="row featured-items blocks-spacer">
            <div class="span12">
                
                <!--  ==========  -->
                <!--  = Title =  -->
                <!--  ==========  -->
                <div class="main-titles lined">
                    <h2 class="title"><span class="light">کتاب های</span> ویژه</h2>
                    <div class="arrows">
                        <a href="#" class="icon-chevron-left" id="featuredItemsLeft"></a>
                        <a href="#" class="icon-chevron-right" id="featuredItemsRight"></a>
                    </div>
                </div>
            </div>
            
            <div class="span12">
                <!--  ==========  -->
                <!--  = Carousel =  -->
                <!--  ==========  -->
                <div class="carouFredSel" data-autoplay="false" data-nav="featuredItems">
                    @yield('contentstar')
                </div> <!-- /carousel -->
            </div>
            
        </div>
    </div> <!-- /container -->
    
    <!--  ==========  -->
    <!--  = New Products =  -->
    <!--  ==========  -->
    <div class="boxed-area blocks-spacer">
        <div class="container">
            
            <!--  ==========  -->
            <!--  = Title =  -->
            <!--  ==========  -->
            <div class="row">
                <div class="span12">
                    <div class="main-titles lined">
                        <h2 class="title"><span class="light">کتاب های</span> جدید فروشگاه</h2>
                    </div>
                </div>
            </div> <!-- /title -->
            
            <div class="row popup-products blocks-spacer">
                @yield('contentnew') 
            </div>
        </div>
    </div> <!-- /new products -->
    
    <!--  ==========  -->
    <!--  = Most Popular products =  -->
    <!--  ==========  -->
    <div class="most-popular blocks-spacer">
        <div class="container">
            
            <!--  ==========  -->
            <!--  = Title =  -->
            <!--  ==========  -->
            <div class="row">
                <div class="span12">
                    <div class="main-titles lined">
                        <h2 class="title"><span class="light">محبوبترین</span>کتاب های فروشگاه</h2>
                    </div>
                </div>
            </div> <!-- /title -->
            
            <div class="row popup-products">
                @yield('contenttop')
            </div>
        </div>
    </div> <!-- /most popular -->
    
    

    
 
     
    <!--  ==========  -->
    <!--  = Footer =  -->
    <!--  ==========  -->
    <footer style="margin-top: 150px;">
        
        <!--  ==========  -->
        <!--  = Upper footer =  -->
        <!--  ==========  -->
        <div class="foot-light">
            <div class="container">
                <div class="row">
                    <div class="span4">
                        <h2 class="pacifico">Bookmarket &nbsp; <img src="<?= Url('themes/1/images/webmarket.png'); ?>" alt="Webmarket Cart" class="align-baseline" /></h2>
                        <p>
                            فروشگاه کتاب ما در زمینه های مختلف کتاب هایی دارد. علاوه بر توانایی خرید شما می توانید مراکز پخش این کتاب را هم مشاهده نمایید.
                        </p>
                    </div>
                    <div class="span4">
                        <div class="main-titles lined">
                            <h3 class="title">ما را در شبکه های اجتماعی دنبال کنید</h3>
                        </div>
                        <div>
                            <div class="icons">
                                <a href="http://www.facebook.com/ProteusNet"><span class="zocial-facebook"></span></a>
                                <a href="https://twitter.com/proteusnetcom"><span class="zocial-twitter"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="main-titles lined">
                            <h3 class="title"><span class="light">عضویت</span> در خبرنامه</h3>
                        </div>
                        <p>با وارد کردن ایمیل خود از آخرین کتاب ها با خبر شوید</p>
                        <!-- Begin MailChimp Signup Form -->
                        <div id="mc_embed_signup">
                        
                        </div>
                        <!--End mc_embed_signup-->
                    </div>
                </div>
            </div>
        </div> <!-- /upper footer -->

        <!--  ==========  -->
        <!--  = Bottom Footer =  -->
        <!--  ==========  -->
        <div class="foot-last">
            <a href="#" id="toTheTop">
                <span class="icon-chevron-up"></span>
            </a>
            <div class="container">
                <div class="row">
                    <div class="span6">
                        &copy; Copyright 2017 
                    </div>
                    <div class="span6">
                        <div class="pull-right">قالب اختصاصی</div>
                    </div>
                </div>
            </div>
        </div> <!-- /bottom footer -->
    </footer> <!-- /footer -->

     
    </div> <!-- end of master-wrapper -->
    


    <!--  ==========  -->
    <!--  = JavaScript =  -->
    <!--  ==========  -->
    
    <!--  = FB =  -->
    
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=126780447403102";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    
    <!--  = jQuery - CDN with local fallback =  -->
    <script type="text/javascript">
    if (typeof jQuery == 'undefined') {
        document.write('<script src=""><\/script>');
    }
    </script>
    

    <script type="text/javascript" src="<?= Url('themes/1/js/jquery.min.js'); ?>"></script>


    <!--  = _ =  -->
    <script src="<?= Url('themes/1/js/underscore/underscore-min.js'); ?>" type="text/javascript"></script>
    
    <!--  = Bootstrap =  -->
    <script src="<?= Url('themes/1/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = Slider Revolution =  -->
    <script src="<?= Url('themes/1/js/rs-plugin/pluginsources/jquery.themepunch.plugins.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= Url('themes/1/js/rs-plugin/js/jquery.themepunch.revolution.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = CarouFredSel =  -->
    <script src="<?= Url('themes/1/js/jquery.carouFredSel-6.2.1-packed.js'); ?>" type="text/javascript"></script>
    
    <!--  = jQuery UI =  -->
    <script src="<?= Url('themes/1/js/jquery-ui-1.10.3/js/jquery-ui-1.10.3.custom.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= Url('themes/1/js/jquery-ui-1.10.3/touch-fix.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = Isotope =  -->
    <script src="<?= Url('themes/1/js/isotope/jquery.isotope.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = Tour =  -->
    <script src="<?= Url('themes/1/js/bootstrap-tour/build/js/bootstrap-tour.min.js'); ?>" type="text/javascript"></script>
    
    <!--  = PrettyPhoto =  -->
    <script src="<?= Url('themes/1/js/prettyphoto/js/jquery.prettyPhoto.js'); ?>" type="text/javascript"></script>
    
    <!--  = Google Maps API =  -->
    <script type="text/javascript" src="<?= Url('themes/1/js/goMap/js/jquery.gomap-1.3.2.min.js'); ?>"></script>
    
    <!--  = Custom JS =  -->
    <script src="<?= Url('themes/1/js/custom.js'); ?>" type="text/javascript"></script>

    @yield('footer')

  </body>
</html>

    
    
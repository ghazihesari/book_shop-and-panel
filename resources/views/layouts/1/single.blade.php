@yield('headers')




    <div class="container">
        <div class="push-up top-equal blocks-spacer">
            <div class="row">
                
                <!--  ==========  -->
                <!--  = Main Title =  -->
                <!--  ==========  -->
                
                @yield('contentsingle')
                


            </div>
        </div>
    </div> <!-- /container -->








     
<!--  ==========  -->
    <!--  = Footer =  -->
    <!--  ==========  -->
    <footer>
        
        <!--  ==========  -->
        <!--  = Upper footer =  -->
        <!--  ==========  -->
        <div class="foot-light">
            <div class="container">
                <div class="row">
                    <div class="span4">
                        <h2 class="pacifico">Bookmarket &nbsp; <img src="<?= Url('themes/1/images/webmarket.png'); ?>" alt="Webmarket Cart" class="align-baseline" /></h2>
                        <p>
                            فروشگاه کتاب ما در زمینه های مختلف کتاب هایی دارد. علاوه بر توانایی خرید شما می توانید مراکز پخش این کتاب را هم مشاهده نمایید.
                        </p>
                    </div>
                    <div class="span4">
                        <div class="main-titles lined">
                            <h3 class="title">ما را در شبکه های اجتماعی دنبال کنید</h3>
                        </div>
                        <div>
                            <div class="icons">
                                <a href="http://www.facebook.com/ProteusNet"><span class="zocial-facebook"></span></a>
                                <a href="https://twitter.com/proteusnetcom"><span class="zocial-twitter"></span></a>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="main-titles lined">
                            <h3 class="title"><span class="light">عضویت</span> در خبرنامه</h3>
                        </div>
                        <p>با وارد کردن ایمیل خود از آخرین کتاب ها با خبر شوید</p>
                        <!-- Begin MailChimp Signup Form -->
                        <div id="mc_embed_signup">
                        
                        </div>
                        <!--End mc_embed_signup-->
                    </div>
                </div>
            </div>
        </div> <!-- /upper footer -->

        <!--  ==========  -->
        <!--  = Bottom Footer =  -->
        <!--  ==========  -->
        <div class="foot-last">
            <a href="#" id="toTheTop">
                <span class="icon-chevron-up"></span>
            </a>
            <div class="container">
                <div class="row">
                    <div class="span6">
                        &copy; Copyright 2017 
                    </div>
                    <div class="span6">
                        <div class="pull-right">قالب اختصاصی</div>
                    </div>
                </div>
            </div>
        </div> <!-- /bottom footer -->
    </footer> <!-- /footer -->



<!--  ==========  -->
<!--  = JavaScript =  -->
<!--  ==========  -->

<!--  = FB =  -->

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=126780447403102";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


<!--  = jQuery - CDN with local fallback =  -->
<script type="text/javascript">
if (typeof jQuery == 'undefined') {
    document.write('<script src=""><\/script>');
}
</script>


<script type="text/javascript" src="<?= Url('themes/1/js/jquery.min.js'); ?>"></script>


<!--  = _ =  -->
<script src="<?= Url('themes/1/js/underscore/underscore-min.js'); ?>" type="text/javascript"></script>

<!--  = Bootstrap =  -->
<script src="<?= Url('themes/1/js/bootstrap.min.js'); ?>" type="text/javascript"></script>

<!--  = Slider Revolution =  -->
<script src="<?= Url('themes/1/js/rs-plugin/pluginsources/jquery.themepunch.plugins.min.js'); ?>" type="text/javascript"></script>
<script src="<?= Url('themes/1/js/rs-plugin/js/jquery.themepunch.revolution.min.js'); ?>" type="text/javascript"></script>

<!--  = CarouFredSel =  -->
<script src="<?= Url('themes/1/js/jquery.carouFredSel-6.2.1-packed.js'); ?>" type="text/javascript"></script>

<!--  = jQuery UI =  -->
<script src="<?= Url('themes/1/js/jquery-ui-1.10.3/js/jquery-ui-1.10.3.custom.min.js'); ?>" type="text/javascript"></script>
<script src="<?= Url('themes/1/js/jquery-ui-1.10.3/touch-fix.min.js'); ?>" type="text/javascript"></script>

<!--  = Isotope =  -->
<script src="<?= Url('themes/1/js/isotope/jquery.isotope.min.js'); ?>" type="text/javascript"></script>

<!--  = Tour =  -->
<script src="<?= Url('themes/1/js/bootstrap-tour/build/js/bootstrap-tour.min.js'); ?>" type="text/javascript"></script>

<!--  = PrettyPhoto =  -->
<script src="<?= Url('themes/1/js/prettyphoto/js/jquery.prettyPhoto.js'); ?>" type="text/javascript"></script>

<!--  = Google Maps API =  -->
<script type="text/javascript" src="<?= Url('themes/1/js/goMap/js/jquery.gomap-1.3.2.min.js'); ?>"></script>

<!--  = Custom JS =  -->
<script src="<?= Url('themes/1/js/custom.js'); ?>" type="text/javascript"></script>

@yield('footer')

</body>
</html>


@extends('layouts.1.checkoutB1')

@section('content')

<!--  ==========  -->
<!--  = Header =  -->
<!--  ==========  -->
<header>
    <div class="row">
        <div class="span2">
            <a href="<?= Url('/'); ?>"><img src="<?= Url('themes/1/images/logo-bw.png'); ?>" alt="فروشگاه کتاب" width="48" height="48" /></a>
        </div>
        <div class="span6">
            <div class="center-align">
                <h1><span class="light">تایید</span> نهایی</h1>
            </div>
        </div>
        <div class="span2">
            <div class="right-align">
                <img src="<?= Url('themes/1/images/buttons/security.jpg'); ?>" alt="امنیت خرید" width="92" height="65" />
            </div>
        </div>
    </div>
</header>

<!--  ==========  -->
<!--  = Steps =  -->
<!--  ==========  -->
<!--  ==========  -->

<div class="checkout-steps">
    <div class="clearfix">
        <div class="step done">
            <div class="step-badge"><i class="icon-ok"></i></div>
            <a href="<?= Url('checkout'); ?>">سبد خريد</a>
        </div>
        <div class="step done">
            <div class="step-badge"><i class="icon-ok"></i></div>
            <a href="<?= Url('checkout-step2'); ?>">آدرس ارسال</a>
        </div>

        <div class="step active">
            <div class="step-badge">4</div>
            تایید نهایی
        </div>
    </div>
</div> <!-- /steps -->

<!--  ==========  -->
<!--  = Selected Items =  -->
<!--  ==========  -->
<table class="table table-items">
    <tbody>
        <tr>
            <td>کد سفارش</td>
            <td> {{ $sef->codesefsresh_sefareshats }} </td>
        </tr>
        <tr>
            <td>کد پیگیری سفارش</td>
            <td> {{ $sef->codepaygiry_sefareshats }} </td>
        </tr>
        <tr>
            <td>آدرس ارسالی</td>
            <td>{{ $sef->address_sefareshats }}</td>
        </tr>
    </tbody>
</table> 

<p class="right-align">
    <a href="<?= Url('/success/sefaresh/'.$sef->id); ?>" class="btn btn-primary higher bold">تاييد نهایی و پرداخت </a>
</p>

@endsection
<?php
    use \App\BooksModel;
?>
@if( !empty( Session::get('cart') ) )
<?php $price = 0; ?>
<div style="height: auto;" class="cart-container" id="cartContainer">


    <div class="open-panel">
         
        
        @foreach ( Session::get('cart') as $key=>$value )
        <div class="item-in-cart clearfix">
            <?php
                
                $book = BooksModel::find( $key );
            ?>
            <div class="image">
                <img src="<?= Url('assets/imgsbook/'.$book->img_book); ?>" width="124" height="124" alt="{{ $book->name_book }}" title="{{ $book->name_book }}"/>
            </div>
            <div class="desc">
                <strong><a href="product.html">{{ $book->name_book }}</a></strong>
                <span class="light-clr qty">
                    تعداد : {{ $value }}
                    &nbsp;
                    <div onclick="delete_book({{ $book->id }})" class="icon-remove-sign" title="حذف یکی از تعداد"></div>
                </span>
            </div>
            <div class="price">
                <strong> ریال {!! number_format($book->price_book) !!} </strong>
                <?php $price+= ($value * $book->price_book) ?>
            </div>
        </div>
        @endforeach


         
        <div class="summary">
            <div class="line">
                <div class="row-fluid">
                    <div class="span6 align-right">60000 ریال</div>
                    <div class="span6">هزینه ارسال :</div>
                </div>
            </div>
            <div class="line">
                <div class="row-fluid">
                    <div class="span6 align-right size-16"> {!! number_format( $price + 60000 ) !!} </div>
                    <div class="span6">جمع کل :</div> 
                </div>
            </div>
        </div>
        <div class="proceed">
            <a href="<?= Url('/checkout'); ?>" class="btn btn-danger pull-right bold higher">تصویه حساب <i class="icon-shopping-cart"></i></a>

            <div onclick="empty_cart()" class="btn btn-danger pull-right bold higher">تخلیه سبد خرید</div>
        </div>
    </div>


    <div class="cart">
        <?php $count = sizeof( Session::get('cart') ); ?>
        <p class="items">سبد خرید <span class="dark-clr">( {{ $count }} )</span></p>
        <p class="dark-clr hidden-tablet">{!! number_format($price) !!}</p>
        <a href="<?= Url('/checkout'); ?>" class="btn btn-danger">
            <!-- <span class="icon icons-cart"></span> -->
            <i class="icon-shopping-cart"></i>
        </a>
    </div>


</div>
@else
    <div class="cart">
        
        <p class="dark-clr hidden-tablet">سبد خرید خالی</p>
        <a href="<?= Url('/checkout'); ?>" class="btn btn-danger">
            <!-- <span class="icon icons-cart"></span> -->
            <i class="icon-shopping-cart"></i>
        </a>
    </div>
@endif
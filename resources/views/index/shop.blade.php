@extends('layouts.1.shop')


@section('headers')
	@extends('index.header')
@endsection


@section('head')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('menu')

	@foreach( $category as $cat )
	<li class="dropdown">
	    <a href="<?= Url('category/'.$cat->name_subjects) ?>" class="dropdown-toggle"> {{ $cat->name_subjects }} <b class="caret"></b> </a>
	    <ul class="dropdown-menu">
	    	@foreach( getzirmenu($cat->id) as $zirmenu )
	        <li class="dropdown">
	            <a href="<?= Url('category/'.$zirmenu->name_subjects) ?>"><i class="pull-right visible-desktop"></i>{{ $zirmenu->name_subjects }}</a>
	        </li>
	        @endforeach
	    </ul>
	</li>
	@endforeach

	<li style="background-color: green;"><a href="<?= Url('/shop'); ?>">فروشگاه</a></li>

@endsection





@section('cart')
	@include('index.cart')
@endsection




@section('contentshop')


	@foreach( $books as $book )
	<!--  ==========  -->
	<!--  = Single Product =  -->
	<!--  ==========  -->
    <div class="span3 filter--underwear" data-price="167" data-popularity="2" data-size="xs|s|l" data-color="blue|pink" data-brand="naish">
        <div class="product">
             	@if( $book->state_book == '1' )
             		<div class="stamp red">تمام شد</div>
	            @else
             	@endif
            <div class="product-img">
                <div class="picture">
                    <img width="540" height="374" alt="{{ $book->name_book }}" title="{{ $book->name_book }}" src="<?= Url('assets/imgsbook/'.$book->img_book); ?>" />
                    <div class="img-overlay">
                        <a class="btn more btn-primary" href="<?= Url('book/'.$book->url_book); ?>">توضیحات بیشتر</a>
                        @if( $book->state_book == '1' || $book->state_book == '2' )
	                    @else
                        <div onclick="add_cart('{{ $book->id }}')" class="btn buy btn-danger">افزودن به سبد خرید</div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="main-titles no-margin">
                 
                <h4 class="title">{{ $book->price_book }}  ریال  </h4>
                 
                <h5 class="no-margin isotope--title"> {{ $book->name_book }} </h5>
            </div>
            <p class="center-align stars">
                <span class="icon-star stars-clr"></span>
                <span class="icon-star stars-clr"></span>
                <span class="icon-star stars-clr"></span>
                <span class="icon-star stars-clr"></span>
                <span class="icon-star stars-clr"></span>
                 
            </p>
        </div> 
    </div> <!-- /single product -->
    @endforeach


@endsection




@section('numberpage')
	{!! $books->render() !!}
@endsection

@section('footer')
<?php $url10 = Url('/add'); ?>
<script type="text/javascript">
	add_cart = function(id)
        {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }});
            $.ajax({
                url:'<?= $url10 ?>',
                type:"POST",
                data:'id_book='+id,
                success:function(data)
                {
                    $("#cart").html(data);
                }
            });
        }
</script>

<?php $url3=Url('/remove'); ?>
<script type="text/javascript">
    delete_book = function(id)
    {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        $.ajax({
            url:'<?= $url3 ?>',
            type:"POST",
            data:'id_book='+id,
            success:function(data)
            {
                $("#cart").html(data);
            }
        });
    }
</script>

<?php $url4=Url('/empty'); ?>
<script type="text/javascript">
    empty_cart = function()
    {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        $.ajax({
            url:'<?= $url4 ?>',
            type:"POST",
            success:function(data)
            {
                $("#cart").html(data);
            }
        });
    }
</script>

@endsection




<?php

use App\SubjectsModel;

function getzirmenu( $id )
{
	$zirmenu = SubjectsModel::where('replay_subjects',$id)->get();
	return $zirmenu;
}
?>

@extends('layouts.1.index')

@section('headers')
	@extends('index.header')
@endsection


@section('head')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('menu')

	@foreach( $category as $cat )
	<li class="dropdown">
	    <a href="<?= Url('category/'.$cat->name_subjects) ?>" class="dropdown-toggle"> {{ $cat->name_subjects }} <b class="caret"></b> </a>
	    <ul class="dropdown-menu">
	    	@foreach( getzirmenu($cat->id) as $zirmenu )
	        <li class="dropdown">
	            <a href="<?= Url('category/'.$zirmenu->name_subjects) ?>"><i class="pull-right visible-desktop"></i>{{ $zirmenu->name_subjects }}</a>
	        </li>
	        @endforeach
	    </ul>
	</li>
	@endforeach
	<li style="background-color: green;"><a href="<?= Url('/shop'); ?>">فروشگاه</a></li>

@endsection





@section('cart')
	@include('index.cart')
@endsection



@section('contentstar')
	@include('index.contentstar')
@endsection


@section('contentnew')
	@foreach( $newcontent as $new )               
	<div class="span3">
	    <div class="product">
	        <div class="product-img">
	            <div class="picture">
	                <img src="<?= Url('assets/imgsbook/'.$new->img_book); ?>" alt="{{ $new->name_book }}" title="{{ $new->name_book }}" width="540" height="374" />
	                <div class="img-overlay">
	                    <a href="<?= Url('book/'.$new->url_book); ?>" class="btn more btn-primary">توضیحات بیشتر</a>
	                    @if( $new->state_book == '1' || $new->state_book == '2' )
	                    @else
                        <div onclick="add_cart('{{ $new->id }}')" class="btn buy btn-danger">افزودن به سبد خرید</div>
                        @endif
	                </div>
	            </div> 
	        </div>
	        <div class="main-titles no-margin">
	            <h4 class="title">  {{ $new->price_book }} ریال </h4>
                <h5 class="no-margin">  کد کتاب  {{ $new->id }} </h5>
	        </div>
	        <p class="desc">{{ $new->name_book }}</p>
	        <p class="center-align stars">
	            <span class="icon-star stars-clr"></span>
	            <span class="icon-star stars-clr"></span>
	            <span class="icon-star stars-clr"></span>
	            <span class="icon-star stars-clr"></span>
	            <span class="icon-star stars-clr"></span> 
	        </p>
	    </div>
	</div> <!-- /product -->
	@endforeach
@endsection





@section('contenttop')

	@foreach( $newtop as $top )
	<div class="span3">
	    <div class="product">
	        <div class="product-img">
	            <div class="picture">
	                <img src="<?= Url('assets/imgsbook/'.$top->img_book); ?>" alt="{{ $top->name_book }}" title="{{ $top->name_book }}" width="540" height="374" />
	                <div class="img-overlay">
	                    <a href="<?= Url('book/'.$top->url_book); ?>" class="btn more btn-primary">توضیحات بیشتر</a>
	                    @if( $top->state_book == '1' || $top->state_book == '2' )
	                    @else
                        <div onclick="add_cart('{{ $top->id }}')" class="btn buy btn-danger">افزودن به سبد خرید</div>
                        @endif
	                </div>
	            </div>
	        </div>
	        <div class="main-titles no-margin">
	            <h4 class="title">  {{ $top->price_book }} ریال </h4>
                <h5 class="no-margin">  کد کتاب  {{ $top->id }} </h5>
	        </div>
	        <p class="desc">{{ $top->name_book }}</p>
	        <p class="center-align stars">
	            <span class="icon-star stars-clr"></span>
	            <span class="icon-star stars-clr"></span>
	            <span class="icon-star stars-clr"></span>
	            <span class="icon-star stars-clr"></span>
	            <span class="icon-star stars-clr"></span>
	             
	        </p>
	    </div>
	</div> <!-- /product -->
	@endforeach
@endsection




@section('footer')
<?php $url10 = Url('/add'); ?>
<script type="text/javascript">
	add_cart = function(id)
        {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }});
            $.ajax({
                url:'<?= $url10 ?>',
                type:"POST",
                data:'id_book='+id,
                success:function(data)
                {
                    $("#cart").html(data);
                }
            });
        }
</script>

<?php $url3=Url('/remove'); ?>
<script type="text/javascript">
    delete_book = function(id)
    {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        $.ajax({
            url:'<?= $url3 ?>',
            type:"POST",
            data:'id_book='+id,
            success:function(data)
            {
                $("#cart").html(data);
            }
        });
    }
</script>

<?php $url4=Url('/empty'); ?>
<script type="text/javascript">
    empty_cart = function()
    {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        $.ajax({
            url:'<?= $url4 ?>',
            type:"POST",
            success:function(data)
            {
                $("#cart").html(data);
            }
        });
    }
</script>

@endsection




<?php

use App\SubjectsModel;
use App\BooksModel;

function getzirmenu( $id )
{
	$zirmenu = SubjectsModel::where('replay_subjects',$id)->get();
	return $zirmenu;
}

function getstar1()
{
	$star1 = BooksModel::where('state_book','3')->orderby('id','desc')->take(3)->get();
	return $star1;
}

function getstar2()
{
	$star2 = BooksModel::where('state_book','3')->orderby('id','desc')->skip(3)->take(3)->get();
	return $star2;
}


?>
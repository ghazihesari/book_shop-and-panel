@extends('layouts.1.checkoutB1')

@section('content')

<!--  ==========  -->
<!--  = Header =  -->
<!--  ==========  -->
<header>
    <div class="row">
        <div class="span2">
            <a href="<?= Url('/'); ?>"><img src="<?= Url('themes/1/images/logo-bw.png'); ?>" alt="فروشگاه کتاب" width="48" height="48" /></a>
        </div>
        <div class="span6">
            <div class="center-align">
                <h1><span class="light"> وارد کردن آدرس پرداخت </span></h1>
            </div>
        </div>
        <div class="span2">
            <div class="right-align">
                <img src="<?= Url('themes/1/images/buttons/security.jpg'); ?>" alt="امنیت خرید" width="92" height="65" />
            </div>
        </div>
    </div>
</header>

<!--  ==========  -->
<!--  = Steps =  -->
<!--  ==========  -->
<div class="checkout-steps">
    <div class="clearfix">
        <div class="step done">
            <div class="step-badge"><i class="icon-ok"></i></div>
            <a href="<?= Url('/checkout'); ?>">سبد خرید</a>
        </div>
        <div class="step active">
            <div class="step-badge">2</div>
            آدرس ارسال
        </div>
        <div class="step">
            <div class="step-badge">3</div>
            تایید و ثبت سفارش
        </div>
    </div>
</div> <!-- /steps -->

<!--  ==========  -->
<!--  = Selected Items =  -->
<!--  ==========  -->

<form action="<?= Url('/checkout'); ?>" method="post" class="form-horizontal form-checkout">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="control-group">
        <label class="control-label" for="name_sefareshats">نام<span class="red-clr bold">*</span></label>
        <div class="controls">
            <input type="text" id="name_sefareshats" name="name_sefareshats" class="span4" required>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="lname_sefareshats">نام خانوادگی<span class="red-clr bold">*</span></label>
        <div class="controls">
            <input type="text" id="lname_sefareshats" name="lname_sefareshats" class="span4" required>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="phone_sefareshats">تلفن<span class="red-clr bold">*</span></label>
        <div class="controls">
            <input type="tel" id="phone_sefareshats" name="phone_sefareshats" class="span4" required>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="mobile_sefareshats">موبایل<span class="red-clr bold">*</span></label>
        <div class="controls">
            <input type="text" id="mobile_sefareshats" name="mobile_sefareshats" class="span4" required>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="email_sefareshats">ایمیل<span class="red-clr bold">*</span></label>
        <div class="controls">
            <input type="email" id="email_sefareshats" name="email_sefareshats" class="span4" required>
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label" for="address_sefareshats">آدرس </label>
        <div class="controls">
            <input type="text" id="address_sefareshats" name="address_sefareshats" class="span4" >
        </div>
    </div>

    <hr />

    <div class="control-group">
        <input type="submit" name="BtnSave" class="btn btn-primary higher bold" value="در مرحله بعدی شما باید اطلاعات خود را ثبت نهایی کنید.">
    </div>
    
</form>



@endsection
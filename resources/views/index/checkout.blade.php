<?php
    use \App\BooksModel;
?>
@extends('layouts.1.checkoutB1')

@section('content')


<!--  ==========  -->
<!--  = Header =  -->
<!--  ==========  -->
<header>
    <div class="row">
        <div class="span2">
            <a href="<?= Url('/'); ?>"><img src="<?= Url('themes/1/images/logo-bw.png'); ?>" alt="فروشگاه کتاب" width="48" height="48" /></a>
        </div>
        <div class="span6">
            <div class="center-align">
                <h1><span class="light">بازبینی</span> سبد خرید</h1>
            </div>
        </div>
        <div class="span2">
            <div class="right-align">
                <img src="<?= Url('themes/1/images/buttons/security.jpg'); ?>" alt="امنیت خرید" width="92" height="65" />
            </div>
        </div>
    </div>
</header>

<!--  ==========  -->
<!--  = Steps =  -->
<!--  ==========  -->
<div class="checkout-steps">
    <div class="clearfix">
        <div class="step active">
            <div class="step-badge">1</div>
            سبد خرید
        </div>
        <div class="step">
            <div class="step-badge">2</div>
            آدرس ارسال
        </div>
        <div class="step">
            <div class="step-badge">3</div>
            تایید و ثبت سفارش
        </div>
    </div>
</div> <!-- /steps -->

<!--  ==========  -->
<!--  = Selected Items =  -->
<!--  ==========  -->
<table class="table table-items">
    <thead>
        <tr>
            <th colspan="2">آیتم</th>
            <th><div class="align-center">تعداد</div></th>
            <th><div class="align-right">قیمت</div></th>
        </tr>
    </thead>
    <tbody>
        
        @if( !empty( Session::get('cart') ) )
            <?php $price = 0; ?>
            @foreach ( Session::get('cart') as $key=>$value )
            <?php  
                $book = BooksModel::find( $key );
            ?>
            <tr>
                <td class="image"><img src="<?= Url('assets/imgsbook/'.$book->img_book); ?>" alt="{{ $book->name_book }}" width="124" height="124" /></td>
                <td class="desc">{{ $book->name_book }} &nbsp; </td>
                <td class="qty">
                    <p> {{ $value }} </p>
                </td>
                <td class="price">
                    {!! number_format($book->price_book * $value) !!}
                    <?php $price+= $book->price_book * $value; ?>
                </td>
            </tr>
            @endforeach
        @else
        @endif

        <tr>
            <td class="stronger">جمع کل :</td>
            <td class="stronger"><div class="size-16 align-right">{!! number_format($price+60000) !!} ریال</div></td>
        </tr>
    </tbody>
</table>


<hr />
                            
<p class="right-align">
    در مرحله بعدی شما آدرس ارسال را وارد خواهید کرد. &nbsp; &nbsp;
    <a href="<?= Url('/checkout-step2') ?>" class="btn btn-primary higher bold">ادامه</a>
</p>

@endsection
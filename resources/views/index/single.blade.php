@extends('layouts.1.single')
@section('headers')
	@extends('index.header')
@endsection
@section('head')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('menu')

	@foreach( $category as $cat )
	<li class="dropdown">
	    <a href="<?= Url('category/'.$cat->name_subjects) ?>" class="dropdown-toggle"> {{ $cat->name_subjects }} <b class="caret"></b> </a>
	    <ul class="dropdown-menu">
	    	@foreach( getzirmenu($cat->id) as $zirmenu )
	        <li class="dropdown">
	            <a href="<?= Url('category/'.$zirmenu->name_subjects) ?>"><i class="pull-right visible-desktop"></i>{{ $zirmenu->name_subjects }}</a>
	        </li>
	        @endforeach
	    </ul>
	</li>
	@endforeach

	<li style="background-color: green;"><a href="<?= Url('/shop'); ?>">فروشگاه</a></li>

@endsection
@section('cart')
	@include('index.cart')
@endsection





@section('contentsingle')

<!--  ==========  -->
<!--  = Main content =  -->
<!--  ==========  -->
<section class="span12 single single-post">
    
    <!--  ==========  -->
    <!--  = Post =  -->
    <!--  ==========  -->
    <article class="post format-video">
        <div class="post-inner">
            
            <div class="post-title">
                <h2><span class="light"> {{ $record->name_book }} </h2>
                <div class="metadata">
                    <span> 
                      {!! countcomment($record->id) !!} نظر 
                    </span> /
                    <span>
                         کاربر : {!! nameuser($record->id_users) !!}  
                    </span>
                </div>
            </div>

            <br/>
            <br/>
            <br/>

            <section id="toggles">
                <h3 class="push-down-20"><span class="light"> در بخش های مختلف می توانید اطلاعات متفاوتی از کتاب را مشاهده نمایید. </span></h3>
                
                <div class="accordion" id="accordion2">
                    <div class="accordion-group accordion-style-2 active">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" href="#collapseOne">
                                <span class="bg-for-icon"><i class="icon-minus"></i><i class="icon-plus"></i></span>
                                تصویر کتاب به همراه توضیحات
                            </a>
                        </div>
                        <div id="collapseOne" class="accordion-body collapse in">
                            <div class="accordion-inner">
                                
                                <section id="gallery">
                                    <h3 class="push-down-20"><span class="light">گرید</span> گالری</h3>
                                    
                                    <div class="row-fluid">   
                                        <div class="span9 push-down-20">
                                            {!! $record->details_book !!}
                                        </div>
                                        
                                        <div class="span3 push-down-20">
                                            <div class="picture">
                                                <img class="rounded" src="<?= Url('assets/imgsbook/'.$record->img_book) ?>" alt="" width="128" height="128" />
                                            </div>
                                        </div>
                                    </div>

                                </section>

                            </div>
                        </div>
                    </div>
                </div>




                <div class="accordion-group accordion-style-2">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" href="#collapse5"> 
                            <span class="bg-for-icon"><i class="icon-minus"></i><i class="icon-plus"></i></span>
                                دسته ای که کتاب در آن قرار گرفته است
                        </a>
                    </div>
                    <div id="collapse5" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <div class="alert alert-success in fade">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                {!! namecategory($record->id_subjects) !!}
                            </div>
                        </div>
                    </div>
                </div>




                <div class="accordion-group accordion-style-2">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" href="#collapse6"> 
                            <span class="bg-for-icon"><i class="icon-minus"></i><i class="icon-plus"></i></span>
                                این کتاب در چه چاپ خانه ای به چاپ رسیده است؟
                        </a>
                    </div>
                    <div id="collapse6" class="accordion-body collapse">
                        <div class="accordion-inner">

                            <?php
                                $chap = chap($record->id_chapkhonehs);
                            ?>
                            <h4><span class="light"> {{ $chap->name_chapkhonehs }} </span></h4>
                             
                            <p>
                                {{ $chap->details_chapkhonehs }}
                            </p>
                            
                            <hr />
                        </div>
                    </div>
                </div>



                <div class="accordion-group accordion-style-2">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseTwo"> 
                            <span class="bg-for-icon"><i class="icon-minus"></i><i class="icon-plus"></i></span>
                                نمایش نویسندگان کتاب
                        </a>
                    </div>
                    <div id="collapseTwo" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <table class="table table-theme table-striped">
                                <thead>
                                    <tr>
                                        <th>نام نویسنده</th>
                                        <th>نام خانوادگی نویسنده</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $moalefs as $moalef )
                                    <tr>  
                                        <td>{!! namemoalef($moalef->id_moalef) !!}</td>
                                        <td>{!! lnamemoalef($moalef->id_moalef) !!}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                           </table>
                        </div>
                    </div>
                </div>


                <div class="accordion-group accordion-style-2">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" href="#collapseThree"> 
                            <span class="bg-for-icon"><i class="icon-minus"></i><i class="icon-plus"></i></span>
                                نمایش مترجمین کتاب
                        </a>
                    </div>
                    <div id="collapseThree" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <table class="table table-theme table-striped">
                                <thead>
                                    <tr>
                                        <th>نام مترجم</th>
                                        <th>نام خانوادگی مترجم</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $motarjems as $motarjem )
                                    <tr>  
                                        <td>{!! namemotarjem($motarjem->id_motarjems) !!}</td>
                                        <td>{!! lnamemotarjem($motarjem->id_motarjems) !!}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                           </table>
                        </div>
                    </div>
                </div>



                <div class="accordion-group accordion-style-2">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" href="#collapse4"> 
                            <span class="bg-for-icon"><i class="icon-minus"></i><i class="icon-plus"></i></span>
                                نمایش مراکز پخش کتاب
                        </a>
                    </div>
                    <div id="collapse4" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <table class="table table-theme table-striped">
                                <thead>
                                    <tr>
                                        <th>نام مرکز پخش</th>
                                        <th>تلفن مرکز پخش</th>
                                        <th> آدرس مرکز پخش </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach( $pakhshs as $pakhsh )
                                    <tr>  
                                        <td>{!! namepakhsh($pakhsh->id_pakhsh) !!}</td>
                                        <td>{!! phonepakhsh($pakhsh->id_pakhsh) !!}</td>
                                        <td>{!! addresspakhsh($pakhsh->id_pakhsh) !!}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                           </table>
                        </div>
                    </div>
                </div>



                <div class="accordion-group accordion-style-2">
                    <div class="accordion-heading">
                        <a class="accordion-toggle" data-toggle="collapse" href="#collapse8"> 
                            <span class="bg-for-icon"><i class="icon-minus"></i><i class="icon-plus"></i></span>
                                مشخصات نوبت چاپ
                        </a>
                    </div>
                    <div id="collapse8" class="accordion-body collapse">
                        <div class="accordion-inner">
                            <table class="table table-theme table-striped">
                                <thead>
                                    <tr>
                                        <th>نوبت چاپ</th>
                                        <th>فصل چاپ</th>
                                        <th>سال چاپ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td> {{ $countprint->count_countprints }} </td>
                                        <td> {{ $countprint->fasle_countprints }} </td>
                                        <td> {{ $countprint->year_countprints }} </td>
                                    </tr>
                                </tbody>
                           </table>
                        </div>
                    </div>
                </div>
                <br/>
                <br/> 
                <div class="center-align">
                    @if( $record->state_book == '1' || $record->state_book == '2' )
                    @else
                        <div onclick="add_cart('{{ $record->id }}')" class="btn btn-success btn-large push-down-10">افزودن به سبد خرید</div>
                    @endif
                </div> 
                <br />



            </div>
        </section>
            
        
                                        
        </div>
    </article>
    
    <hr />
    
    <!--  ==========  -->
    <!--  = Comments =  -->
    <!--  ==========  -->
    
    <section id="comments" class="comments-container">
        <h3 class="push-down-25"><span class="light">{!! countcomment($record->id) !!}</span> نظر</h3>
        
        <!--  ==========  -->
        <!--  = Single Comment =  -->
        <!--  ==========  -->
        @foreach( $comments as $comment )
        <div class="single-comment clearfix">
            <div class="comment-content">
                <div class="comment-inner">
                    <cite class="author-name">
                        <span class="light"> {{ $comment->name_comments }} </span> 
                    </cite>
                    <div class="comment-text">
                        <p>
                            {{ $comment->content_comments }}
                        </p>
                    </div>
                </div>
                
                 
                 <?php
                    $recomments = replaycomments($comment->id);
                 ?>
                <!--  ==========  -->
                <!--  = Single Nested Comment - one level =  -->
                <!--  ==========  -->
                @foreach( $recomments as $recomment )
                <div class="single-comment nested clearfix">
                    <div class="comment-content">
                        <div class="comment-inner">
                            <cite class="author-name">
                                <span class="light"> {{ $recomment->name_comments }} </span>
                            </cite>
                            <div class="comment-text">
                                <p>
                                    {{ $recomment->content_comments }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div> <!-- /nested comment -->
                @endforeach
            </div>
        </div>
        @endforeach
        
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        <br/>
        
        <hr />
        
        <h3 class="push-down-25"><span class="light">نظر</span> بدهید</h3>
        
        <form id="commentform" method="post" action="<?= Url('/comment') ?>" class="form form-inline form-comments">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_books" value="{{ $record->id }}">
            <p class="push-down-20">
                <label for="name_comments">نام<span class="red-clr bold">*</span></label>
                <input type="text" aria-required="true" tabindex="1" size="30" id="name_comments" name="name_comments" required>
            </p>
            <p class="push-down-20">
                <label for="lname_comments">نام خانوادگی</label>
                <input type="text" tabindex="2" size="30" id="lname_comments" name="lname_comments">
            </p>
            <p class="push-down-20">
                <label for="email_comments">ایمیل<span class="red-clr bold">*</span></label>
                <input type="email" aria-required="true" tabindex="3" size="30" id="email_comments" name="email_comments" required>
            </p>
            

            <p class="push-down-20">
                <textarea class="input-block-level" tabindex="4" rows="7" cols="70" id="content_comments" name="content_comments" placeholder="نظرتان را در اینجا بنویسید ..." required></textarea>
            </p>
            <p>
                <button class="btn btn-primary bold" type="submit" tabindex="5" id="submit">ارسال نظر</button>
            </p>
        </form>
        
    </section>

</section> <!-- /main content -->
@endsection




@section('footer')
<?php $url10 = Url('/add'); ?>
<script type="text/javascript">
	add_cart = function(id)
        {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }});
            $.ajax({
                url:'<?= $url10 ?>',
                type:"POST",
                data:'id_book='+id,
                success:function(data)
                {
                    $("#cart").html(data);
                }
            });
        }
</script>

<?php $url3=Url('/remove'); ?>
<script type="text/javascript">
    delete_book = function(id)
    {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        $.ajax({
            url:'<?= $url3 ?>',
            type:"POST",
            data:'id_book='+id,
            success:function(data)
            {
                $("#cart").html(data);
            }
        });
    }
</script>

<?php $url4=Url('/empty'); ?>
<script type="text/javascript">
    empty_cart = function()
    {
        $.ajaxSetup({
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }});
        $.ajax({
            url:'<?= $url4 ?>',
            type:"POST",
            success:function(data)
            {
                $("#cart").html(data);
            }
        });
    }
</script>

@endsection




<?php

use App\SubjectsModel;
use App\CommentModel;
use App\User;
use App\ChapKhonehModel;
use App\NevisandehModel;
use App\MotarjemModel;
use App\PakhshModel;



function replaycomments( $id )
{
    $recomments = CommentModel::where(['replaye_comments'=>$id,'state'=>1])->get();
    return $recomments;
}


function namepakhsh( $id )
{
    $name = PakhshModel::where('id',$id)->first()['name_pakhsh'];
    return $name;
}

function phonepakhsh( $id )
{
    $name = PakhshModel::where('id',$id)->first()['phone_pakhsh'];
    return $name;
}

function addresspakhsh( $id )
{
    $name = PakhshModel::where('id',$id)->first()['address_pakhsh'];
    return $name;
}





function namemoalef( $id )
{
    $name = NevisandehModel::where('id',$id)->first()['name_moalefs'];
    return $name;
}

function lnamemoalef( $id )
{
    $name = NevisandehModel::where('id',$id)->first()['lname_moalefs'];
    return $name;
}

function namemotarjem( $id )
{
    $name = MotarjemModel::where('id',$id)->first()['name_motarjems'];
    return $name;
}

function lnamemotarjem( $id )
{
    $name = MotarjemModel::where('id',$id)->first()['lname_motarjems'];
    return $name;
}

function getzirmenu( $id )
{
	$zirmenu = SubjectsModel::where('replay_subjects',$id)->get();
	return $zirmenu;
}

function countcomment( $id )
{
    $count = CommentModel::where(['id_books'=>$id,'state'=>1])->count();
    return $count;
}
function nameuser( $id )
{
    $name = User::where('id',$id)->first()['fname'];
    return $name;
}

function namecategory( $id )
{
    $name = SubjectsModel::where('id',$id)->first()['name_subjects'];
    return $name;
}

function chap( $id )
{
    $name = ChapKhonehModel::find($id);
    return $name;
}

?>

@extends('layouts.adminLayouts')

@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> اضافه کردن مشخصات نویسنده جدید </h1> 
	</div>
</div>
<hr/>

<div class="row">
	<div class="col-lg-12" >


		@if( $errors->has('name_moalefs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('name_moalefs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('lname_moalefs') )
		<div class="panel-body no-margin-top">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('lname_moalefs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('details_moalefs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('details_moalefs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('website_moalefs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('website_moalefs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('phone_moalefs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('phone_moalefs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('address_moalefs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('address_moalefs') !!}
		    </div>
		</div>
		@endif




		{!! Form::open(['url'=>'admin/moalef','class'=>'form-horizontal']) !!}

			<div class="form-group">
				{{ Form::label('name_moalefs','نام نویسنده جدید',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('name_moalefs',null,['class'=>'form-control','placeholder'=>'نام نویسنده جدید را وارد کنید']) }}
				</div>
			</div>


			<div class="form-group">
				{{ Form::label('lname_moalefs','نام خانوادگی نویسنده جدید',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('lname_moalefs',null,['class'=>'form-control','placeholder'=>'نام خانوادگی نویسنده را وارد نمایید.']) }}
				</div>
			</div>



			<div class="form-group">
				{{ Form::label('details_moalefs','توضیحات نویسنده',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::textarea('details_moalefs',null,['class'=>'form-control','placeholder'=>'توضیحاتی برای نویسنده وارد نمایید.']) }}
				</div>
			</div>


			{{ Form::label('website_moalefs','وب سایت نویسنده',['class'=>'control-label col-lg-3']) }}
			<div class="form-group input-group col-lg-7" style="margin-right: 1px;">
				{{ Form::text('website_moalefs',null,['class'=>'form-control','placeholder'=>'آدرس وب سایت نویسنده را وارد نمایید.']) }}
				<span class="input-group-addon">http://www</span>
			</div>

 
			<div class="form-group">
				{{ Form::label('phone_moalefs','تلفن نویسنده',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('phone_moalefs',null,['class'=>'form-control','placeholder'=>'تلفن نویسنده را وارد نمایید.']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('address_moalefs','آدرس نویسنده',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::textarea('address_moalefs',null,['class'=>'form-control','id'=>'cleditor','placeholder'=>'آدرس نویسنده را وارد نمایید.']) }}
				</div>
			</div>

			<div class="form-actions" style="text-align:center;margin-bottom:80px;">
			    {{ Form::submit('ثبت اطلاعات نویسنده جدید',['class'=>'btn btn-primary btn-lg']) }}
			</div>

        {!! Form::close() !!}

	</div>
</div>


@endsection

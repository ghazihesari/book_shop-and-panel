@extends('layouts.adminLayouts')

@section('content')
<div class="col-lg-12">
	<div class="row" >
		<h1> مشاهده و مدیریت تمامی نویسنده های موجود </h1> 
	</div>
</div>
<hr/>


<div class="row" style="margin-top: 30px;">

	@foreach( $nevisandehs as $nevisandeh1 )
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                {{ $nevisandeh1->name_moalefs }}
            </div>
            <div class="panel-body">
                <p> تلفن : {{ $nevisandeh1->	phone_moalefs }} </p>
                <br/>
                <p> آدرس : {{ $nevisandeh1->address_moalefs }} </p>
                <br/>
                <p> توضیحات : {{ $nevisandeh1->details_moalefs }} </p>
            </div>
            <div class="panel-footer" align="center">

                <button type="button" title="نمایش" class="btn btn-primary btn-circle btn-lg" data-toggle="modal" data-target="#{{ $nevisandeh1->id }}"><i class="icon-list"></i>
                </button>

                <a href="<?= Url('admin/moalef/'.$nevisandeh1->id.'/edit') ?>" title="ویرایش" class="btn btn-success btn-circle btn-lg"><i class="icon-link"></i>
                </a>

                <button type="button" title="حذف" class="btn btn-danger btn-circle btn-lg" data-toggle="modal" data-target="#delete{{ $nevisandeh1->id }}"><i class="icon-bitbucket"></i>
                </button>

            </div>
        </div>
    </div>
    @endforeach

    <div class="row">
        <div class="col-lg-12">
        	@foreach( $nevisandehs as $nevisandeh2 )
        	<div class="modal" id="{{ $nevisandeh2->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title" id="myModalLabel">{{ $nevisandeh2->name_moalefs }}</h4>
		                </div>
		                <div class="modal-body">
		                    <p> تلفن : {{ $nevisandeh2->phone_moalefs }} </p>
					        <br/>
					        <p> وب سایت : <a href="http://www.{{ $nevisandeh2->website_moalefs }}" target="_balck">{{ $nevisandeh2->website_moalefs }}</a> </p>
					        <br/>
					        <p> آدرس : {{ $nevisandeh2->address_moalefs }} </p>
					        <br/>
					        <p> توضیحات : {{ $nevisandeh2->details_moalefs }} </p>
		                </div>
		            </div>
		        </div>
		    </div>
		    @endforeach
        </div>





        <div class="col-lg-12">
        	@foreach( $nevisandehs as $nevisandeh3 )
	        <div class="modal fade" id="delete{{ $nevisandeh3->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title" id="H2"> آیا از حذف کردن {{ $nevisandeh3->name_moalefs }} نویسنده اطمینان دارید ؟؟ </h4>
	                    </div>
	                    <div class="modal-body">

	                    	<p> {{ $nevisandeh3->details_moalefs }} </p>
		                    
	                    </div>
	                    <div class="modal-footer">
	                        
	                        <form action="<?= Url('admin/moalef/'.$nevisandeh3->id); ?>" method="POST">
		                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">  
		                    	<input type="hidden" name="_method" value="DELETE">

		                    	<input type="submit" name="btndelete" value="حذف کردن" class="btn btn-danger">
		                	</form>

	                        <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>

	                    </div>
	                </div>
	            </div>
	        </div>
	        @endforeach
        </div>







    </div>


</div>

<div class="row">
	<div class="col-lg-12" align="center">
		{!! $nevisandehs->render() !!}
	</div>
</div>




@endsection
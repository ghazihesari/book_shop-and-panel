@extends('layouts.adminLayouts')

@section('content')
<div class="col-lg-12">
	<div class="row" >
		<h1> مشاهده و مدیریت تمامی مترجمین موجود </h1> 
	</div>
</div>
<hr/>


<div class="row" style="margin-top: 30px;">

	@foreach( $motarjem as $motarjem1 )
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading"> 
                {{ $motarjem1->name_motarjems }}
            </div>
            <div class="panel-body">
            	<p> نام خانوادگی : {{ $motarjem1->lname_motarjems }} </p>
                <br/>
                <p> تلفن : {{ $motarjem1->phone_motarjems }} </p>
                <br/>
                <p> مقطع تحصیلی : {{ $motarjem1->maghtah_motarjems }} </p>
                <br/>
                <p> رشته تحصیلی : {{ $motarjem1->reshtah_motarjems }} </p>
                <br/>
                <p> توضیحات : {{ $motarjem1->details_motarjems }} </p>
                <br/>

            </div>
            <div class="panel-footer" align="center">

                <button type="button" title="نمایش" class="btn btn-primary btn-circle btn-lg" data-toggle="modal" data-target="#{{ $motarjem1->id }}"><i class="icon-list"></i>
                </button>

                <a href="<?= Url('admin/motarjems/'.$motarjem1->id.'/edit') ?>" title="ویرایش" class="btn btn-success btn-circle btn-lg"><i class="icon-link"></i>
                </a>

                <button type="button" title="حذف" class="btn btn-danger btn-circle btn-lg" data-toggle="modal" data-target="#delete{{ $motarjem1->id }}"><i class="icon-bitbucket"></i>
                </button>

            </div>
        </div>
    </div>
    @endforeach

    <div class="row">
        <div class="col-lg-12">
        	@foreach( $motarjem as $motarjem2 )
        	<div class="modal" id="{{ $motarjem2->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title" id="myModalLabel">{{ $motarjem2->name_motarjems }}</h4>
		                </div>
		                <div class="modal-body">
		                	<p> نام : {{ $motarjem1->name_motarjems }} </p>
		                	<br/>
		                    <p> نام خانوادگی : {{ $motarjem1->lname_motarjems }} </p>
			                <br/>
			                <p> تلفن : {{ $motarjem1->phone_motarjems }} </p>
			                <br/>
			                <p> مقطع تحصیلی : {{ $motarjem1->maghtah_motarjems }} </p>
			                <br/>
			                <p> رشته تحصیلی : {{ $motarjem1->reshtah_motarjems }} </p>
			                <br/>
			                
							<p> سن : {{ $motarjem1->age_motarjems }} </p>
			                <br/>
			                <p> ایمیل : {{ $motarjem1->email_motarjems }} </p>
			                <br/>
			                <p> سایت : {{ $motarjem1->website_motarjems }} </p>
			                <br/>
			                

			                <p> توضیحات : {{ $motarjem1->details_motarjems }} </p>
			                <br/>
		                </div>
		            </div>
		        </div>
		    </div>
		    @endforeach
        </div>





        <div class="col-lg-12">
        	@foreach( $motarjem as $motarjem3 )
	        <div class="modal fade" id="delete{{ $motarjem3->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title" id="H2"> آیا از حذف کردن {{ $motarjem3->name_motarjems }} نویسنده اطمینان دارید ؟؟ </h4>
	                    </div>
	                    <div class="modal-body">

	                    	<p> {{ $motarjem3->details_motarjems }} </p>
		                    
	                    </div>
	                    <div class="modal-footer">
	                        
	                        <form action="<?= Url('admin/motarjems/'.$motarjem3->id); ?>" method="POST">
		                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">  
		                    	<input type="hidden" name="_method" value="DELETE">

		                    	<input type="submit" name="btndelete" value="حذف کردن" class="btn btn-danger">
		                	</form>

	                        <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>

	                    </div>
	                </div>
	            </div>
	        </div>
	        @endforeach
        </div>







    </div>


</div>

<div class="row">
	<div class="col-lg-12" align="center">
		{!! $motarjem->render() !!}
	</div>
</div>




@endsection
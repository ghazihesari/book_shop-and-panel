@extends('layouts.adminLayouts')

@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> اضافه کردن مشخصات مترجم جدید </h1> 
	</div>
</div>
<hr/>

<div class="row">
	<div class="col-lg-12" >
 

		@if( $errors->has('name_motarjems') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('name_motarjems') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('lname_motarjems') )
		<div class="panel-body no-margin-top">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('lname_motarjems') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('age_motarjems') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('age_motarjems') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('maghtah_motarjems') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('maghtah_motarjems') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('reshtah_motarjems') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('reshtah_motarjems') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('phone_motarjems') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('phone_motarjems') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('details_motarjems') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('details_motarjems') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('email_motarjems') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('email_motarjems') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('website_motarjems') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('website_motarjems') !!}
		    </div>
		</div>
		@endif




		{!! Form::open(['url'=>'admin/motarjems','class'=>'form-horizontal']) !!}

			<div class="form-group">
				{{ Form::label('name_motarjems','نام مترجم',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('name_motarjems',null,['class'=>'form-control','placeholder'=>'نام مترجم جدید را وارد کنید']) }}
				</div>
			</div> 


			<div class="form-group">
				{{ Form::label('lname_motarjems','نام خانوادگی مترجم',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('lname_motarjems',null,['class'=>'form-control','placeholder'=>'نام خانوادگی مترجم را وارد نمایید.']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('age_motarjems','سن مترجم',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('age_motarjems',null,['class'=>'form-control','placeholder'=>'سن مترجم را وارد نمایید.']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('maghtah_motarjems','مقطع تحصیلی',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('maghtah_motarjems',null,['class'=>'form-control','placeholder'=>'مقطع تحصیلی مترجم را وارد نمایید.']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('reshtah_motarjems','رشته تحصیلی',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('reshtah_motarjems',null,['class'=>'form-control','placeholder'=>'رشته تحصیلی مترجم را وارد نمایید.']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('phone_motarjems','تلفن مترجم',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('phone_motarjems',null,['class'=>'form-control','placeholder'=>'تلفن مترجم را وارد نمایید.']) }}
				</div>
			</div>


			{{ Form::label('website_motarjems','وب سایت مترجم',['class'=>'control-label col-lg-3']) }}
			<div class="form-group input-group col-lg-7" style="margin-right: 1px;">
				{{ Form::text('website_motarjems',null,['class'=>'form-control','placeholder'=>'آدرس وب سایت مترجم را وارد نمایید.']) }}
				<span class="input-group-addon">http://www</span>
			</div>


			{{ Form::label('email_motarjems','ایمیل مترجم',['class'=>'control-label col-lg-3']) }}
			<div class="form-group input-group col-lg-7" style="margin-right: 1px;">
				<span class="input-group-addon">@</span>
				{{ Form::text('email_motarjems',null,['class'=>'form-control','placeholder'=>'ایمیل مترجم را وارد نمایید.']) }}
			</div>


			<div class="form-group">
				{{ Form::label('details_motarjems','توضیحات اضافی',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::textarea('details_motarjems',null,['class'=>'form-control','placeholder'=>'توضیحاتی اضافی برای مترجم وارد نمایید.']) }}
				</div>
			</div>

			<div class="form-actions" style="text-align:center;margin-bottom:80px;">
			    {{ Form::submit('ثبت اطلاعات مترجم جدید',['class'=>'btn btn-primary btn-lg']) }}
			</div>

        {!! Form::close() !!}

	</div>
</div>


@endsection

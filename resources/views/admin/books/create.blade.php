@extends('layouts.adminLayouts')



@section('head')
<link rel="stylesheet" href="<?= Url('assets/css/bootstrap-fileupload.min.css'); ?>" />
@endsection


@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> افزودن کتاب جدید  </h1> 
	</div>
</div>
<hr/>

<div class="row">
	<div class="col-lg-12" >


	{!! Form::open(['url'=>'admin/books','class'=>'form-horizontal','files'=>true]) !!}

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                مشخصات کتاب را وارد نمایید.     
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">			                    
		                	
		                	<div class="form-group">
								{{ Form::label('name_book','نام کتاب',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::text('name_book',null,['class'=>'form-control','placeholder'=>'نام کتاب را وارد نمایید']) }}
								</div>
							</div>


							<div class="form-group">
								{{ Form::label('count_book','تعداد موجودی',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::text('count_book',null,['class'=>'form-control','placeholder'=>'تعداد کتاب موجودی را وارد نمایید']) }}
								</div>
							</div>


							<div class="form-group">
								{{ Form::label('price_book','قیمت کتاب',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::text('price_book',null,['class'=>'form-control','placeholder'=>'قیمت کتاب را وارد نمایید']) }}
								</div>
							</div>


							<div class="form-group">
								{{ Form::label('shabook_book','شابک',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::text('shabook_book',null,['class'=>'form-control','placeholder'=>'شابک کتاب را وارد نمایید']) }}
								</div>
							</div>
				 
							<div class="form-group">
								{{ Form::label('details_book','توضیحات کتاب',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::textarea('details_book',null,['class'=>'ckeditor','placeholder'=>'توضیحات کتاب را وارد نمایید.']) }}
								</div>
							</div>

		                </div>
		            </div>
		        </div>
		    </div>
		</div>


		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                چاپ خانه ای که در آن چاپ شده است را انتخاب کنید      
		                <a href="<?= Url('admin/chapkhoneh/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
		              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن چاپ خانه جدید">
		              </a>     
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">			                    
		                	
		                	<div class="form-group">
								{{ Form::label('id_chapkhonehs','انتخاب چاپ خانه',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::select('id_chapkhonehs',$chaps,null,['class'=>'form-control']) }}
								</div>
							</div>

		                </div>
		            </div>
		        </div>
		    </div>
		</div>
		
		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                 کتاب در چه موضوعی می باشد؟    
		                 <a href="<?= Url('admin/category/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
		              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن موضوع جدید">
		              </a>     
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">			                    
		                	<div class="form-group">
								{{ Form::label('id_subjects','انتخاب موضوع کتاب',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::select('id_subjects',$categorys,null,['class'=>'form-control']) }}
								</div>
							</div>

		                </div>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                   تصویری برای کتاب انتخاب نمایید.    
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">			                    
		                	<div class="form-group">
				                <label class="control-label col-lg-4">آپلود عکس</label>
				                <div class="col-lg-8">
				                    <div class="fileupload fileupload-new" data-provides="fileupload">
				                        <div class="fileupload-preview thumbnail" style="width: 350px; height: 250px;"></div>
				                        <div>
				                            <span class="btn btn-file btn-success"><span class="fileupload-new">انتخاب عکس</span><span class="fileupload-exists">تغییر </span><input type="file" name="imgbook" /></span>
				                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">حذف</a>
				                        </div>
				                    </div>
				                </div>
				            </div>
		                </div>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                 نویسنده گان کتاب خود را انتخاب کنید.  
		                 <a href="<?= Url('admin/moalef/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
		              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن نویسنده جدید">
		              </a>     
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">			                    
		                	

		                	<div class="row">

		                		@foreach( $nevisandes as $moalef )
				                <div class="col-lg-4">
				                    <div class="well">
				                        <h4>
				                        	{{ $moalef->name_moalefs }} {{ $moalef->lname_moalefs }}  
				                        	<input type="checkbox" name="nevis[]" value="{{ $moalef->id }}">
				                        </h4>
				                        <p>
				                        	{{ $moalef->details_moalefs }}
				                        </p>
				                    </div>
				                </div>
				                @endforeach

				            </div>

		                </div>
		            </div>
		        </div>
		    </div>
		</div>


		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		                مترجمان کتاب خود را انتخاب کنید.     
		                <a href="<?= Url('admin/motarjems/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
		              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن مترجم جدید">
		              </a> 
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">			                    
		                	

		                	<div class="row">

		                		@foreach( $motarjems as $motarjem )
				                <div class="col-lg-4">
				                    <div class="well">
				                        <h4>
				                        	{{ $motarjem->name_motarjems }} {{ $motarjem->lname_motarjems }}  
				                        	<input type="checkbox" name="motajs[]" value="{{ $motarjem->id }}">
				                        </h4>
				                        <p>
				                        	{{ $motarjem->details_motarjems }}
				                        </p>
				                    </div>
				                </div>
				                @endforeach

				            </div>

		                </div>
		            </div>
		        </div>
		    </div>
		</div>


		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		              مراکزی که کتاب در آن ها پخش شده است انتخاب نمایید. 
		              <a href="<?= Url('admin/pakhsh/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
		              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن مرکز پخش جدید">
		              </a> 
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">			                    
		                	
		                	<div class="row">

		                		@foreach( $pakhshs as $pakhsh )
				                <div class="col-lg-4">
				                    <div class="well">
				                        <h4>
				                        	{{ $pakhsh->name_pakhsh }}   
				                        	<input type="checkbox" name="pakhshselects[]" value="{{ $pakhsh->id }}">
				                        </h4>
				                        <p>
				                        	<span style="color:red;"> آدرس : </span> {{ $pakhsh->address_pakhsh }}
				                        </p>
				                        <p>
				                        	<span style="color:red;"> توضیحات : </span> {{ $pakhsh->details_pakhsh }}
				                        </p>
				                    </div>
				                </div>
				                @endforeach

				            </div>


		                </div>
		            </div>
		        </div>
		    </div>
		</div>







		<div class="row">
		    <div class="col-lg-12">
		        <div class="panel panel-default">
		            <div class="panel-heading">
		               مشخصات چاپ کتاب
		            </div>
		            <div class="panel-body">
		                <div class="table-responsive">			                    
		                	
		                	<div class="form-group">
								{{ Form::label('count_countprints','نوبت چاپ',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::text('count_countprints',null,['class'=>'form-control','placeholder'=>'نوبت چاپ را وارد نمایید.']) }}
								</div>
							</div>

							<div class="form-group">
								{{ Form::label('fasle_countprints','فصل چاپ',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::text('fasle_countprints',null,['class'=>'form-control','placeholder'=>'فصل چاپ را وارد نمایید.']) }}
								</div>
							</div>

							<div class="form-group">
								{{ Form::label('year_countprints','سال چاپ',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::text('year_countprints',null,['class'=>'form-control','placeholder'=>'سال چاپ را وارد نمایید.']) }}
								</div>
							</div>

							<div class="form-group">
								{{ Form::label('moneth_countprints','ماه چاپ',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::text('moneth_countprints',null,['class'=>'form-control','placeholder'=>'ماه چاپ را وارد نمایید.']) }}
								</div>
							</div>

							<div class="form-group">
								{{ Form::label('countbook_countprints','تعداد نسخه',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::text('countbook_countprints',null,['class'=>'form-control','placeholder'=>'تعداد نسخه چاپ شده']) }}
								</div>
							</div>


							<div class="form-group">
								{{ Form::label('details_countprints','توضیحات اضافی',['class'=>'control-label col-lg-3']) }}
								<div class="col-lg-7">
									{{ Form::textarea('details_countprints',null,['class'=>'form-control','placeholder'=>'توضیحاتی اضافی در رابطه با این نوبت چاپ']) }}
								</div>
							</div>

		                </div>
		            </div>
		        </div>
		    </div>
		</div>





		<div class="form-actions" style="text-align:center;margin-bottom:80px;">
			{{ Form::submit('ثبت اطلاعات کتاب ',['class'=>'btn btn-primary btn-lg']) }}
		</div>

		{!! Form::close() !!}

	</div>
</div>


@endsection




@section('script')
<script src="<?= Url('assets/plugins/jasny/js/bootstrap-fileupload.js'); ?>"></script>
<script src="<?= Url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
@endsection
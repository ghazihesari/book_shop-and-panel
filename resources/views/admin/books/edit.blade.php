@extends('layouts.adminLayouts')



@section('head')
<link rel="stylesheet" href="<?= Url('assets/css/bootstrap-fileupload.min.css'); ?>" />
@endsection



@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> ویرایش کتاب <span style="color:red;"> {{ $record->name_book }} </span>  </h1> 
	</div>
</div>
<hr/>

<div class="row">
	<div class="col-lg-12" >


		{!! Form::model( $record , [ 'method'=>'PATCH' , 'route'=>['admin.books.update', $record->id ],'class'=>'form-horizontal','files'=>true ] ) !!}

			<div class="row">
			    <div class="col-lg-12">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			                مشخصات کتاب را وارد نمایید.     
			            </div>
			            <div class="panel-body">
			                <div class="table-responsive">			                    
			                	
			                	<div class="form-group">
									{{ Form::label('name_book','نام کتاب',['class'=>'control-label col-lg-3']) }}
									<div class="col-lg-7">
										{{ Form::text('name_book',null,['class'=>'form-control','placeholder'=>'نام کتاب را وارد نمایید']) }}
									</div>
								</div>


								<div class="form-group">
									{{ Form::label('count_book','تعداد موجودی',['class'=>'control-label col-lg-3']) }}
									<div class="col-lg-7">
										{{ Form::text('count_book',null,['class'=>'form-control','placeholder'=>'تعداد کتاب موجودی را وارد نمایید']) }}
									</div>
								</div>


								<div class="form-group">
									{{ Form::label('price_book','قیمت کتاب',['class'=>'control-label col-lg-3']) }}
									<div class="col-lg-7">
										{{ Form::text('price_book',null,['class'=>'form-control','placeholder'=>'قیمت کتاب را وارد نمایید']) }}
									</div>
								</div>


								<div class="form-group">
									{{ Form::label('shabook_book','شابک',['class'=>'control-label col-lg-3']) }}
									<div class="col-lg-7">
										{{ Form::text('shabook_book',null,['class'=>'form-control','placeholder'=>'شابک کتاب را وارد نمایید']) }}
									</div>
								</div>
					 
								<div class="form-group">
									{{ Form::label('details_book','توضیحات کتاب',['class'=>'control-label col-lg-3']) }}
									<div class="col-lg-7">
										{{ Form::textarea('details_book',null,['class'=>'ckeditor','placeholder'=>'توضیحات کتاب را وارد نمایید.']) }}
									</div>
								</div>

								<div class="form-group">
									{{ Form::label('state_book','انتخاب وضعیت کتاب',['class'=>'control-label col-lg-3']) }}
									<div class="col-lg-7">
										{{ Form::select('state_book',$state,null,['class'=>'form-control']) }}
									</div>
								</div>

			                </div>
			            </div>
			        </div>
			    </div>
			</div>



			<div class="row">
			    <div class="col-lg-12">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			                چاپ خانه ای که در آن چاپ شده است را انتخاب کنید      
			                <a href="<?= Url('admin/chapkhoneh/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
			              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن چاپ خانه جدید">
			              </a>     
			            </div>
			            <div class="panel-body">
			                <div class="table-responsive">			                    
			                	
			                	<div class="form-group">
									{{ Form::label('id_chapkhonehs','انتخاب چاپ خانه',['class'=>'control-label col-lg-3']) }}
									<div class="col-lg-7">
										{{ Form::select('id_chapkhonehs',$chaps,null,['class'=>'form-control']) }}
									</div>
								</div>

			                </div>
			            </div>
			        </div>
			    </div>
			</div>
			
			<div class="row">
			    <div class="col-lg-12">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			                 کتاب در چه موضوعی می باشد؟    
			                 <a href="<?= Url('admin/category/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
			              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن موضوع جدید">
			              </a>     
			            </div>
			            <div class="panel-body">
			                <div class="table-responsive">			                    
			                	<div class="form-group">
									{{ Form::label('id_subjects','انتخاب موضوع کتاب',['class'=>'control-label col-lg-3']) }}
									<div class="col-lg-7">
										{{ Form::select('id_subjects',$categorys,null,['class'=>'form-control']) }}
									</div>
								</div>

			                </div>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="row">
			    <div class="col-lg-12">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			                   تصویری برای کتاب انتخاب نمایید.    
			            </div>
			            <div class="panel-body">
			                <div class="table-responsive">			                    
			                	<div class="form-group">
					                <label class="control-label col-lg-4">آپلود عکس</label>
					                <div class="col-lg-8">
					                    <div class="fileupload fileupload-new" data-provides="fileupload">
					                        <div class="fileupload-preview thumbnail" style="width: 350px; height: 250px;"></div>
					                        <div>
					                            <span class="btn btn-file btn-success"><span class="fileupload-new">انتخاب عکس</span><span class="fileupload-exists">تغییر </span><input type="file" name="imgbook" /></span>
					                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload">حذف</a>
					                        </div>
					                    </div>
					                </div>
					            </div>
			                </div>
			            </div>
			        </div>
			    </div>
			</div>

			<div class="row">
			    <div class="col-lg-12">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			                 نویسنده گان کتاب خود را انتخاب کنید.  
			                 <a href="<?= Url('admin/moalef/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
			              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن نویسنده جدید">
			              </a>     
			            </div>
			            <div class="panel-body">
			                <div class="table-responsive">			                    
			                	

			                	<div class="row">

			                		@foreach( $nevisandes as $moalef )
					                <div class="col-lg-4">
					                    <div class="well">
					                        <h4>
					                        	{{ $moalef->name_moalefs }} {{ $moalef->lname_moalefs }}  
					                        	<input type="checkbox"
					                        	<?php
					                        		if ( getnevisandeh($record->id,$moalef->id) ) {
					                        			echo 'checked="checked"';
					                        		}
					                        	?>
					                        	name="nevis[]" value="{{ $moalef->id }}">
					                        </h4>
					                        <p>
					                        	{{ $moalef->details_moalefs }}
					                        </p>
					                    </div>
					                </div>
					                @endforeach

					            </div>

			                </div>
			            </div>
			        </div>
			    </div>
			</div>


			<div class="row">
			    <div class="col-lg-12">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			                مترجمان کتاب خود را انتخاب کنید.     
			                <a href="<?= Url('admin/motarjems/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
			              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن مترجم جدید">
			              </a> 
			            </div>
			            <div class="panel-body">
			                <div class="table-responsive">			                    
			                	

			                	<div class="row">

			                		@foreach( $motarjems as $motarjem )
					                <div class="col-lg-4">
					                    <div class="well">
					                        <h4>
					                        	{{ $motarjem->name_motarjems }} {{ $motarjem->lname_motarjems }}  
					                        	<input type="checkbox"
					                        	<?php
					                        		if ( getstatemotarjem($record->id,$motarjem->id) ) {
					                        			
					                        			echo "checked='checked'";
					                        		}
					                        	?>
					                        	name="motajs[]" value="{{ $motarjem->id }}">
					                        </h4>
					                        <p>
					                        	{{ $motarjem->details_motarjems }}
					                        </p>
					                    </div>
					                </div>
					                @endforeach

					            </div>

			                </div>
			            </div>
			        </div>
			    </div>
			</div>


			<div class="row">
			    <div class="col-lg-12">
			        <div class="panel panel-default">
			            <div class="panel-heading">
			              مراکزی که کتاب در آن ها پخش شده است انتخاب نمایید. 
			              <a href="<?= Url('admin/pakhsh/create'); ?>" target="_black" style="float: left;margin-left: -35px;">
			              	<img src="<?= Url('assets/img/Add.png'); ?>" width="40px" height="40px" title="افزودن مرکز پخش جدید">
			              </a> 
			            </div>
			            <div class="panel-body">
			                <div class="table-responsive">			                    
			                	
			                	<div class="row">

			                		@foreach( $pakhshs as $pakhsh )
					                <div class="col-lg-4">
					                    <div class="well">
					                        <h4>
					                        	{{ $pakhsh->name_pakhsh }}   
					                        	<input type="checkbox" 
					                        	<?php
					                        		if ( getstatepakhsh($record->id,$pakhsh->id) ) {
					                        			
					                        			echo "checked='checked'";
					                        		}
					                        	?>
					                        	name="pakhshselects[]" value="{{ $pakhsh->id }}">
					                        </h4>
					                        <p>
					                        	<span style="color:red;"> آدرس : </span> {{ $pakhsh->address_pakhsh }}
					                        </p>
					                        <p>
					                        	<span style="color:red;"> توضیحات : </span> {{ $pakhsh->details_pakhsh }}
					                        </p>
					                    </div>
					                </div>
					                @endforeach

					            </div>


			                </div>
			            </div>
			        </div>
			    </div>
			</div>






			<div class="form-actions" style="text-align:center;margin-bottom:80px;">
			    {{ Form::submit('ثبت اطلاعات ویرایش شده',['class'=>'btn btn-primary btn-lg']) }}
			</div>

        {!! Form::close() !!}

	</div>
</div>


@endsection



@section('script')
<script src="<?= Url('assets/plugins/jasny/js/bootstrap-fileupload.js'); ?>"></script>
<script src="<?= Url('assets/plugins/ckeditor/ckeditor.js'); ?>"></script>
@endsection

<?php 

use App\NevisanehAndBookModel;
use App\BookAndMotarjemModel;
use App\PakhshBookModel;


function getnevisandeh( $book_id , $nevisandeh_id )
{
	$test = NevisanehAndBookModel::where(['id_books'=>$book_id,'id_moalef'=>$nevisandeh_id])->count();
	if ( $test != 0 ) {
		return true;
	}else{
		return false;
	}
}

function getstatemotarjem( $book_id , $motarjem_id )
{
	$test = BookAndMotarjemModel::where(['id_book'=>$book_id,'id_motarjems'=>$motarjem_id])->count();
	if ( $test != 0 ) {
		return true;
	}else{
		return false;
	}
}

function getstatepakhsh( $book_id , $pakhsh_id )
{
	$test = PakhshBookModel::where(['id_book'=>$book_id,'id_pakhsh'=>$pakhsh_id])->count();
	if ( $test != 0 ) {
		return true;
	}else{
		return false;
	}
}



?>
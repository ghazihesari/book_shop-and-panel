@extends('layouts.adminLayouts')

@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> نمایش و مدیریت کتاب های سایت  <a href="<?= Url('admin/books/create'); ?>" class="btn btn-primary btn-sm btn-line btn-rect">افزودن کتاب جدید </a> </h1> 
	</div>
</div>
<hr/>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                تمامی کتاب های سایت
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>نام کتاب</th>
                                <th>تعداد موجودی </th>
                                <th>قیمت کتاب</th>
                                <th>کاربر ثبت کننده</th>
                                <th> تصویر کتاب  </th>
                                <th> وضعیت کتاب </th>
                                <th> تعداد بازدید </th>
                                <th> عملیات مورد نظر </th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach( $books as $book )
                            <tr class="gradeC">
                                <td> {{ $book->name_book }} </td>
                                <td> {{ $book->count_book }} </td>
                                <td> {{ $book->price_book }} </td>
                                <td> {!! getusername($book->id_users) !!} </td>
                                <td>
                                    <img src="<?= Url('assets/imgsbook/'.$book->img_book); ?>" width="60" height="60" alt="{{ $book->name_book }}" title="{{ $book->name_book }}"> 
                                </td>
                                @if( $book->state_book == '0' )
                                    <td style="color: black;background: yellow;"> وضعیت نرمال </td>
                                @elseif( $book->state_book == '1' )
                                    <td style="color: black;background: orange;"> ناموجود  </td>
                                @elseif( $book->state_book == '2' )
                                    <td style="color: black;background: blue;"> فروش ممنوع  </td>
                                @elseif( $book->state_book == '3' )
                                    <td style="color: black;background: green;"> کتاب ویژه </td>
                                @else
                                    <td style="color: black;background: red;"> نامعلوم </td>
                                @endif

                                <td> {{ $book->view_book }} </td>
                                <td>  
                                	<a href="<?= Url('admin/books/'.$book->id.'/edit'); ?>" class="btn btn-primary btn-sm btn-line">ویرایش</a>
                    				<a href="#" class="btn btn-danger btn-sm btn-line" data-toggle="modal" data-target="#delete{{ $book->id }}">حذف</a>
	                            </td>
                            </tr>
                            @endforeach
 
                        </tbody>
                    </table>
                </div>
               
            </div>

            {!! $books->render() !!}

        </div>
    </div>
</div>


<div class="row">
	<div class="col-lg-12">
    	@foreach( $books as $book )
        <div class="modal fade" id="delete{{ $book->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="H2"> آیا از حذف کردن کتاب {{ $book->name_book }}  اطمینان دارید ؟؟ </h4>
                    </div>
                    <div class="modal-body">

                    	<img src="<?= Url('assets/imgsbook/'.$book->img_book); ?>" width="200" height="200" alt="{{ $book->name_book }}" title="{{ $book->name_book }}">
	                    
                    </div>
                    <div class="modal-footer">
                        
                        <form action="<?= Url('admin/books/'.$book->id); ?>" method="POST">
	                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">  
	                    	<input type="hidden" name="_method" value="DELETE">

	                    	<input type="submit" name="btndelete" value="حذف کردن" class="btn btn-danger">
	                	</form>

                        <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>

                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


@endsection


<?php

use App\User;

function getusername( $id )
{
    $username = User::find($id)->first()['name'];
    return $username;
}

?>
@extends('layouts.adminLayouts')

@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> نمایش و مدیریت دیدگاه ها </h1> 
	</div>
</div>
<hr/>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                تمامی دیدگاه های موجود
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>کد دیدگاه</th>
                                <th>نام </th>
                                <th>نام خانوادگی</th>
                                <th>ایمیل</th>
                                <th> محتویات  </th>
                                <th> در پاسخ به دیدگاه </th>
                                <th> وضعیت دیدگاه </th>
                                <th> برای کتاب </th>
                                <th> عملیات </th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach( $comments as $comment )
                            <tr>
                                <td>{{ $comment->id }}</td>
                                <td>{{ $comment->name_comments }} </td>
                                <td>{{ $comment->lname_comments }}</td>
                                <td>{{ $comment->email_comments }}</td>
                                <td> {{ $comment->content_comments }}  </td>
                                <td> {!! contentcomment($comment->replaye_comments) !!} </td>
 
                                @if( $comment->state == '0' )
                                <td style="background-color: red;"> بررسی و تایید نشده </td>
                                @else
                                <td style="background-color: green;color:#fff;"> تایید شده است </td>
                                @endif

                                <td>
                                    <?php  $book = book( $comment->id_books ); ?>
                                    <img src="<?= Url('assets/imgsbook/'.$book->img_book); ?>" width="60" height="60" alt="{{ $book->name_book }}" title="{{ $book->name_book }}">  
                                </td>
 
                                <td>  
                                    <a href="<?= Url('admin/comments/'.$comment->id.'/edit'); ?>" class="btn btn-primary btn-sm btn-line">پاسخ دادن</a>
                                    <a href="<?= Url('admin/comments/'.$comment->id); ?>" class="btn btn-danger btn-sm btn-line">حذف</a>

                                    <a href="<?= Url('admin/comments/success/'.$comment->id); ?>" class="btn btn-success btn-sm btn-line">تایید دیدگاه</a>
                                </td>

                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
               
            </div>

            {!! $comments->render() !!}

        </div>
    </div>
</div>

@endsection

<?php

use App\CommentModel;
use App\BooksModel;

function contentcomment( $id )
{
    $name = CommentModel::where('id',$id)->first()['content_comments'];
    return $name;
}


function book( $id )
{
    $book = BooksModel::find( $id );
    return $book;
}


?>
@extends('layouts.adminLayouts')

@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> اضافه کردن مشخصات چاپ خانه جدید </h1> 
	</div>
</div>
<hr/>

<div class="row">
	<div class="col-lg-12" >


		@if( $errors->has('name_chapkhonehs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('name_chapkhonehs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('year_chapkhonehs') )
		<div class="panel-body no-margin-top">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('year_chapkhonehs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('phone_chapkhonehs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('phone_chapkhonehs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('email_chapkhonehs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('email_chapkhonehs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('website_chapkhonehs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('website_chapkhonehs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('address_chapkhonehs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('address_chapkhonehs') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('details_chapkhonehs') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('details_chapkhonehs') !!}
		    </div>
		</div>
		@endif




		{!! Form::open(['url'=>'admin/chapkhoneh','class'=>'form-horizontal']) !!}

			<div class="form-group">
				{{ Form::label('name_chapkhonehs','نام چاپ خانه جدید',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('name_chapkhonehs',null,['class'=>'form-control','placeholder'=>'نام چاپ خانه جدید را وارد کنید']) }}
				</div>
			</div>


			<div class="form-group">
				{{ Form::label('year_chapkhonehs','سال تاسیس',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('year_chapkhonehs',null,['class'=>'form-control','placeholder'=>'سال ساخت را وارد نمایید.']) }}
				</div>
			</div>



			<div class="form-group">
				{{ Form::label('phone_chapkhonehs','تلفن چاپ خانه',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('phone_chapkhonehs',null,['class'=>'form-control','placeholder'=>'تلفن چاپ خانه را وارد نمایید.']) }}
				</div>
			</div>


			{{ Form::label('email_chapkhonehs','ایمیل چاپ خانه',['class'=>'control-label col-lg-3']) }}
			<div class="form-group input-group col-lg-7" style="margin-right: 1px;">
				<span class="input-group-addon">@</span>
				{{ Form::text('email_chapkhonehs',null,['class'=>'form-control','placeholder'=>'ایمیل چاپ خانه را وارد نمایید.']) }}
			</div>
 

 			{{ Form::label('website_chapkhonehs','وب سایت چاپ خانه',['class'=>'control-label col-lg-3']) }}
			<div class="form-group input-group col-lg-7" style="margin-right: 1px;">
				{{ Form::text('website_chapkhonehs',null,['class'=>'form-control','placeholder'=>'وب سایت چاپ خانه را وارد نمایید.']) }}
				<span class="input-group-addon">http://www</span>
			</div>

			<div class="form-group">
				{{ Form::label('address_chapkhonehs','آدرس چاپ خانه',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::textarea('address_chapkhonehs',null,['class'=>'form-control','id'=>'cleditor','placeholder'=>'آدرس چاپ خانه را وارد نمایید.']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('details_chapkhonehs','توضیحات چاپ خانه',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::textarea('details_chapkhonehs',null,['class'=>'form-control','id'=>'cleditor','placeholder'=>'توضیحاتی اضافی برای چاپ خانه وارد نمایید.']) }}
				</div>
			</div>

			<div class="form-actions" style="text-align:center;margin-bottom:80px;">
			    {{ Form::submit('ثبت اطلاعات چاپ خانه ',['class'=>'btn btn-primary btn-lg']) }}
			</div>

        {!! Form::close() !!}

	</div>
</div>


@endsection

@extends('layouts.adminLayouts')

@section('content')
<div class="col-lg-12">
	<div class="row" >
		<h1> مشاهده و مدیریت تمامی چاپ خانه های موجود </h1> 
	</div>
</div>
<hr/>


<div class="row" style="margin-top: 30px;">

	@foreach( $chap as $chap1 )
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                {{ $chap1->name_chapkhonehs }}
            </div> 
            <div class="panel-body">
                <p> تلفن : {{ $chap1->phone_chapkhonehs }} </p>
                <br/>
                <p> آدرس : {{ $chap1->address_chapkhonehs }} </p>
                <br/>
                <p> توضیحات : {{ $chap1->details_chapkhonehs }} </p>
            </div>
            <div class="panel-footer" align="center">

                <button type="button" title="نمایش" class="btn btn-primary btn-circle btn-lg" data-toggle="modal" data-target="#{{ $chap1->id }}"><i class="icon-list"></i>
                </button>

                <a href="<?= Url('admin/chapkhoneh/'.$chap1->id.'/edit') ?>" title="ویرایش" class="btn btn-success btn-circle btn-lg"><i class="icon-link"></i>
                </a>

                <button type="button" title="حذف" class="btn btn-danger btn-circle btn-lg" data-toggle="modal" data-target="#delete{{ $chap1->id }}"><i class="icon-bitbucket"></i>
                </button>

            </div>
        </div>
    </div>
    @endforeach

    <div class="row">
        <div class="col-lg-12">
        	@foreach( $chap as $chap2 )
        	<div class="modal" id="{{ $chap2->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title" id="myModalLabel">{{ $chap2->name_chapkhonehs }}</h4>
		                </div>
		                <div class="modal-body">
		                	<p> سال تاسیس : {{ $chap2->year_chapkhonehs }} </p>
					        <br/>
		                    <p> تلفن : {{ $chap2->phone_chapkhonehs }} </p>
					        <br/>
					        <p> ایمیل : {{ $chap2->email_chapkhonehs }} </p>
					        <br/>
					        <p> وب سایت : <a href="http://www.{{ $chap2->website_chapkhonehs }}" target="_balck">{{ $chap2->website_chapkhonehs }}</a> </p>
					        <br/>
					        <p> آدرس : {{ $chap2->address_chapkhonehs }} </p>
					        <br/>
					        <p> توضیحات : {{ $chap2->details_chapkhonehs }} </p>
		                </div>
		            </div>
		        </div>
		    </div>
		    @endforeach
        </div>





        <div class="col-lg-12">
        	@foreach( $chap as $chap3 )
	        <div class="modal fade" id="delete{{ $chap3->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title" id="H2"> آیا از حذف کردن {{ $chap3->name_chapkhonehs }} نویسنده اطمینان دارید ؟؟ </h4>
	                    </div>
	                    <div class="modal-body">

	                    	<p> {{ $chap3->details_chapkhonehs }} </p>
		                    
	                    </div>
	                    <div class="modal-footer">
	                        
	                        <form action="<?= Url('admin/chapkhoneh/'.$chap3->id); ?>" method="POST">
		                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">  
		                    	<input type="hidden" name="_method" value="DELETE">

		                    	<input type="submit" name="btndelete" value="حذف کردن" class="btn btn-danger">
		                	</form>

	                        <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>

	                    </div>
	                </div>
	            </div>
	        </div>
	        @endforeach
        </div>







    </div>


</div>

<div class="row">
	<div class="col-lg-12" align="center">
		{!! $chap->render() !!}
	</div>
</div>




@endsection
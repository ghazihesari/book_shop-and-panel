@extends('layouts.adminLayouts')
@section('content')


<div class="col-lg-12">
	<div class="row" >
		<h1> تمامی مراکز فروش ثبت شده </h1> 
	</div>
</div>
<hr/>


<div class="row" style="margin-top: 30px;">


	@foreach( $pakhsh as $pakhsh1 )
	<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                {{ $pakhsh1->name_pakhsh }}
            </div>
            <div class="panel-body">
                <p> تلفن : {{ $pakhsh1->phone_pakhsh }} </p>
                <br/>
                <p> آدرس : {{ $pakhsh1->address_pakhsh }} </p>
                <br/>
                <p> توضیحات : {{ $pakhsh1->details_pakhsh }} </p>
            </div>
            <div class="panel-footer" align="center">

                <button type="button" title="نمایش" class="btn btn-primary btn-circle btn-lg" data-toggle="modal" data-target="#{{ $pakhsh1->id }}"><i class="icon-list"></i>
                </button>

                <a href="<?= Url('admin/pakhsh/'.$pakhsh1->id.'/edit') ?>" title="ویرایش" class="btn btn-success btn-circle btn-lg"><i class="icon-link"></i>
                </a>

                <button type="button" title="حذف" class="btn btn-danger btn-circle btn-lg" data-toggle="modal" data-target="#delete{{ $pakhsh1->id }}"><i class="icon-bitbucket"></i>
                </button>

            </div>
        </div>
    </div>
    @endforeach

    <div class="row">
        <div class="col-lg-12">
        	@foreach( $pakhsh as $pakhsh2 )
        	<div class="modal" id="{{ $pakhsh2->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		        <div class="modal-dialog">
		            <div class="modal-content">
		                <div class="modal-header">
		                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                    <h4 class="modal-title" id="myModalLabel">{{ $pakhsh2->name_pakhsh }}</h4>
		                </div>
		                <div class="modal-body">
		                    <p> تلفن : {{ $pakhsh2->phone_pakhsh }} </p>
					        <br/>
					        <p> شماره فکس : {{ $pakhsh2->fax_pakhsh }} </p>
					        <br/>
					        <p> ایمیل : {{ $pakhsh2->email_pakhsh }} </p>
					        <br/>
					        <p> وب سایت : <a href="http://www.{{ $pakhsh2->website_pakhsh }}" target="_balck">{{ $pakhsh2->website_pakhsh }}</a> </p>
					        <br/>
					        <p> آدرس : {{ $pakhsh2->address_pakhsh }} </p>
					        <br/>
					        <p> توضیحات : {{ $pakhsh2->details_pakhsh }} </p>
		                </div>
		            </div>
		        </div>
		    </div>
		    @endforeach
        </div>





        <div class="col-lg-12">
        	@foreach( $pakhsh as $pakhsh3 )
	        <div class="modal fade" id="delete{{ $pakhsh3->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	            <div class="modal-dialog">
	                <div class="modal-content">
	                    <div class="modal-header">
	                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                        <h4 class="modal-title" id="H2"> آیا از حذف کردن {{ $pakhsh3->name_pakhsh }} اطمینان دارید ؟؟ </h4>
	                    </div>
	                    <div class="modal-body">

	                    	<p> {{ $pakhsh3->details_pakhsh }} </p>
		                    
	                    </div>
	                    <div class="modal-footer">
	                        
	                        <form action="<?= Url('admin/pakhsh/'.$pakhsh3->id); ?>" method="POST">
		                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">  
		                    	<input type="hidden" name="_method" value="DELETE">

		                    	<input type="submit" name="btndelete" value="حذف کردن" class="btn btn-danger">
		                	</form>

	                        <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>

	                    </div>
	                </div>
	            </div>
	        </div>
	        @endforeach
        </div>







    </div>


</div>

<div class="row">
	<div class="col-lg-12" align="center">
		{!! $pakhsh->render() !!}
	</div>
</div>

@endsection
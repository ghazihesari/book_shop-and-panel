@extends('layouts.adminLayouts')

@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> ویرایش اطلاعات مرکز پخش <span style="color:red;font-size: 35px;"> {{ $record->name_pakhsh }} </span> </h1> 
	</div>
</div>
<hr/>

<div class="row">
	<div class="col-lg-12" >


		@if( $errors->has('name_pakhsh') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('name_pakhsh') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('phone_pakhsh') )
		<div class="panel-body no-margin-top">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('phone_pakhsh') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('fax_pakhsh') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('fax_pakhsh') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('email_pakhsh') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('email_pakhsh') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('website_pakhsh') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('website_pakhsh') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('address_pakhsh') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('address_pakhsh') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('details_pakhsh') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('details_pakhsh') !!}
		    </div>
		</div>
		@endif



		{!! Form::model( $record , [ 'method'=>'PATCH' , 'route'=>['admin.pakhsh.update', $record->id ] ] ) !!}

			<div class="form-group">
				{{ Form::label('name_pakhsh','نام مرکز پخش',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('name_pakhsh',null,['class'=>'form-control','placeholder'=>'نام مرکز پخش را وارد کنید']) }}
				</div>
			</div>


			<div class="form-group">
				{{ Form::label('phone_pakhsh','تلفن مرکز پخش',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('phone_pakhsh',null,['class'=>'form-control','placeholder'=>'تلفن مرکز پخش را وارد کنید']) }}
				</div>
			</div>



			<div class="form-group">
				{{ Form::label('fax_pakhsh','شماره فکس',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('fax_pakhsh',null,['class'=>'form-control','placeholder'=>'شماره فکس را وارد نمایید']) }}
				</div>
			</div>



			{{ Form::label('email_pakhsh','ایمیل مرکز پخش',['class'=>'control-label col-lg-3']) }}
			<div class="form-group input-group col-lg-7" style="margin-right: 1px;">
				<span class="input-group-addon" >@</span>
				{{ Form::text('email_pakhsh',null,['class'=>'form-control','placeholder'=>'ایمیل مرکز پخش را وارد نمایید']) }}	
			</div>


			{{ Form::label('website_pakhsh','نشانی وب',['class'=>'control-label col-lg-3']) }}
			<div class="form-group input-group col-lg-7" style="margin-right: 1px;">
				{{ Form::text('website_pakhsh',null,['class'=>'form-control','placeholder'=>'نشانی وب را وارد نمایید']) }}
				<span class="input-group-addon">http://www</span>
			</div>


			<div class="form-group">
				{{ Form::label('address_pakhsh','آدرس مرکز پخش',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::textarea('address_pakhsh',null,['class'=>'form-control','placeholder'=>'آدرس مرکز پخش را وارد نمایید']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('details_pakhsh','توضیحات',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::textarea('details_pakhsh',null,['class'=>'form-control','id'=>'cleditor','placeholder'=>'توضیحاتی برای مرکز پخش وارد نمایید']) }}
				</div>
			</div>

			<div class="form-actions" style="text-align:center;margin-bottom:80px;">
			    {{ Form::submit('ثبت اطلاعات مرکز پخش',['class'=>'btn btn-primary btn-lg']) }}
			</div>

        {!! Form::close() !!}

		

	</div>
</div>


@endsection

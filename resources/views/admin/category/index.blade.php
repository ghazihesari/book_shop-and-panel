@extends('layouts.adminLayouts')

@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> نمایش و مدیریت تمامی موضوعات ثبت شده  </h1> 
	</div>
</div>
<hr/>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                تمامی موضوعات ثبت شده
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>کد موضوع</th>
                                <th>نام موضوع</th>
                                <th>توضیحات موضوع</th>
                                <th>زیر دسته ای از</th>
                                <th> عملیات </th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            @foreach( $categorys as $cat )
                            <tr class="gradeC">
                                <td> {{ $cat->id }} </td>
                                <td> {{ $cat->name_subjects }} </td>
                                <td> {{ $cat->details_subjects }} </td>
                                <td class="center"> {!! catname( $cat->replay_subjects ) !!} </td>
                                <td>  
                                	<a href="<?= Url('admin/category/'.$cat->id.'/edit'); ?>" class="btn btn-primary btn-sm btn-line">ویرایش</a>
                    				<a href="#" class="btn btn-danger btn-sm btn-line" data-toggle="modal" data-target="#delete{{ $cat->id }}">حذف</a>
	                            </td>
                            </tr>
                            @endforeach
 
                        </tbody>
                    </table>
                </div>
               
            </div>

            {!! $categorys->render() !!}

        </div>
    </div>
</div>


<div class="row">
	<div class="col-lg-12">
    	@foreach( $categorys as $cat )
        <div class="modal fade" id="delete{{ $cat->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="H2"> آیا از حذف کردن موضوع {{ $cat->	name_subjects }}  اطمینان دارید ؟؟ </h4>
                    </div>
                    <div class="modal-body">

                    	<p> {{ $cat->details_subjects }} </p>
	                    
                    </div>
                    <div class="modal-footer">
                        
                        <form action="<?= Url('admin/category/'.$cat->id); ?>" method="POST">
	                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">  
	                    	<input type="hidden" name="_method" value="DELETE">

	                    	<input type="submit" name="btndelete" value="حذف کردن" class="btn btn-danger">
	                	</form>

                        <button type="button" class="btn btn-default" data-dismiss="modal">انصراف</button>

                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>


@endsection

<?php
use App\SubjectsModel;

function catname( $id )
{
	if ( $id == '-' ) {
		return '-';
	}
	else{
		$name = SubjectsModel::where('id',$id)->first()['name_subjects'];
		return $name;
	}
}


?>
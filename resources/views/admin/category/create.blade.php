@extends('layouts.adminLayouts')

@section('content')

<div class="col-lg-12">
	<div class="row" >
		<h1> اضافه کردن موضوع جدید  </h1> 
	</div>
</div>
<hr/>

<div class="row">
	<div class="col-lg-12" >


		@if( $errors->has('name_subjects') )
		<div class="panel-body">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('name_subjects') !!}
		    </div>
		</div>
		@endif
		@if( $errors->has('details_subjects') )
		<div class="panel-body no-margin-top">
		    <div class="alert alert-danger alert-dismissable">
		        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		            {!! $errors->first('details_subjects') !!}
		    </div>
		</div>
		@endif




		{!! Form::open(['url'=>'admin/category','class'=>'form-horizontal']) !!}

			<div class="form-group">
				{{ Form::label('name_subjects','نام موضوع',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::text('name_subjects',null,['class'=>'form-control','placeholder'=>'نام موضوع را وارد نمایید']) }}
				</div>
			</div>

 
			<div class="form-group">
				{{ Form::label('details_subjects','توضیحات موضوع',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::textarea('details_subjects',null,['class'=>'form-control','placeholder'=>'توضیحاتی در رابطه با موضوع بیان کنید']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('replay_subjects','انتخاب دسته مادر',['class'=>'control-label col-lg-3']) }}
				<div class="col-lg-7">
					{{ Form::select('replay_subjects',$category,null,['class'=>'form-control']) }}
				</div>
			</div>

			<div class="form-actions" style="text-align:center;margin-bottom:80px;">
			    {{ Form::submit('ثبت اطلاعات موضوع جدید',['class'=>'btn btn-primary btn-lg']) }}
			</div>

        {!! Form::close() !!}

	</div>
</div>


@endsection

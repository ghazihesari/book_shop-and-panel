@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">ثبت نام</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">نام کاربری</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">ایمیل</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">گذرواژه</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">تکرار گذرواژه</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="fname" class="col-md-4 control-label">نام</label>

                            <div class="col-md-6">
                                <input type="text" name="fname" class="form-control">
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="lname" class="col-md-4 control-label">نام خانوادگی</label>
                            <div class="col-md-6">
                                <input type="text" name="lname" class="form-control">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="mobile" class="col-md-4 control-label">موبایل</label>
                            <div class="col-md-6">
                                <input type="text" name="mobile" class="form-control">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="phone" class="col-md-4 control-label">تلفن ثابت</label>
                            <div class="col-md-6">
                                <input type="text" name="phone" class="form-control">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="code" class="col-md-4 control-label">کد پستی</label>
                            <div class="col-md-6">
                                <input type="text" name="code" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="col-md-4 control-label">آدرس</label>
                            <div class="col-md-6">
                                <input type="text" name="address" class="form-control">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="details" class="col-md-4 control-label">توضیحات</label>
                            <div class="col-md-6">
                                <input type="text" name="details" class="form-control">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> ثبت نام
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ChapKhonehModel;
use App\SubjectsModel;
use App\Http\Requests;
use App\NevisandehModel;
use App\MotarjemModel;
use App\PakhshModel;
use App\BooksModel;
use App\NevisanehAndBookModel;
use App\BookAndMotarjemModel;
use App\PakhshBookModel;
use App\NobatChapModel;
use Auth;

class BooksController extends Controller
{

    public function __construct()
    {
        if ( Auth::check() ) {
            $this->middleware('AdminMiddle');
        }else{
            $this->middleware('auth');
        }
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = BooksModel::orderby('id','desc')->paginate(12);
        return View('admin.books.index',['books'=>$books]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $chaps = [''=>'چاپ خانه را انتخاب نمایید.'] + ChapKhonehModel::orderby('id','desc')->lists('name_chapkhonehs','id')->toArray();

        $categorys = [''=>'موضوع کتاب را انتخاب نمایید.'] + SubjectsModel::orderby('id','desc')->lists('name_subjects','id')->toArray();

        $nevisandes = NevisandehModel::orderby('id','desc')->get();

        $motarjems = MotarjemModel::orderby('id','desc')->get();

        $pakhshs = PakhshModel::orderby('id','desc')->get();

        return View('admin.books.create',['chaps'=>$chaps,'categorys'=>$categorys,'nevisandes'=>$nevisandes,'motarjems'=>$motarjems,'pakhshs'=>$pakhshs]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $book = new BooksModel( $request->all() );
        $book->id_users = Auth::user()->id;
        $book->state_book = 0;
        $book->view_book = 0;
        $title = str_replace('-','',$request->name_book);
        $book->url_book = preg_replace('/\s+/','-',$title);

        if ( $request->hasFile('imgbook') ) {
            $FileName = time().'.'.$request->file('imgbook')->getClientOriginalExtension();
            if ( $request->file('imgbook')->move( 'assets/imgsbook',$FileName ) ) {
                $book->img_book = $FileName;
            }
        }

        if ( $book->save() ) {
            
            if ( $request->has('nevis') ) {
                foreach ($request->get('nevis') as $key => $value) {
                    $nevisandeh = NevisanehAndBookModel::create(['id_books'=>$book->id,'id_moalef'=>$value]);
                }
            }
            

            if ( $request->has('motajs') ) {
                foreach ($request->get('motajs') as $key => $value) {
                    $motaj = BookAndMotarjemModel::create(['id_book'=>$book->id,'id_motarjems'=>$value]);
                }
            }


            if ( $request->has('pakhshselects') ) {
                foreach ($request->get('pakhshselects') as $key => $value) {
                    $pakhsh = PakhshBookModel::create(['id_book'=>$book->id,'id_pakhsh'=>$value]);
                }
            }

            $nobate = NobatChapModel::create(['count_countprints'=>$request->count_countprints,'fasle_countprints'=>$request->fasle_countprints,'year_countprints'=>$request->year_countprints,'moneth_countprints'=>$request->moneth_countprints,'countbook_countprints'=>$request->countbook_countprints,'details_countprints'=>$request->details_countprints,'id_books'=>$book->id]);


            return redirect('admin/books');

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $chaps = [''=>'چاپ خانه را انتخاب نمایید.'] + ChapKhonehModel::orderby('id','desc')->lists('name_chapkhonehs','id')->toArray();

        $categorys = [''=>'موضوع کتاب را انتخاب نمایید.'] + SubjectsModel::orderby('id','desc')->lists('name_subjects','id')->toArray();

        $nevisandes = NevisandehModel::orderby('id','desc')->get();

        $motarjems = MotarjemModel::orderby('id','desc')->get();

        $pakhshs = PakhshModel::orderby('id','desc')->get();

        $record = BooksModel::find($id);

        $state = ['0'=>'کتاب در حالت نرمال','1'=>'کتاب در انبار موجودی ندارد','2'=>' اجازه خرید به کاربران نده','3'=>'کتاب در لیست ویژه قرار بگیرد'];

        return View('admin.books.edit',['record'=>$record,'chaps'=>$chaps,'categorys'=>$categorys,'nevisandes'=>$nevisandes,'motarjems'=>$motarjems,'pakhshs'=>$pakhshs,'state'=>$state]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $book = BooksModel::find($id);
        $title = str_replace('-','',$request->name_book);
        $book->url_book = preg_replace('/\s+/','-',$title);

        if ( $request->hasFile('imgbook') ) {
            $FileName = time().'.'.$request->file('imgbook')->getClientOriginalExtension();
            if ( $request->file('imgbook')->move( 'assets/imgsbook',$FileName ) ) {
                $book->img_book = $FileName;
            }
        }

        if ( $book->update( $request->all() ) ) {
            
            if ( $request->has('nevis') ) {
                $delete = NevisanehAndBookModel::where('id_books',$id)->delete();
                foreach ($request->get('nevis') as $key => $value) {
                    $nevisandeh = NevisanehAndBookModel::create(['id_books'=>$book->id,'id_moalef'=>$value]);
                }
            }
            

            if ( $request->has('motajs') ) {
                $delete2 = BookAndMotarjemModel::where('id_book',$id)->delete();
                foreach ($request->get('motajs') as $key => $value) {
                    $motaj = BookAndMotarjemModel::create(['id_book'=>$book->id,'id_motarjems'=>$value]);
                }
            }


            if ( $request->has('pakhshselects') ) {
                $delete3 = PakhshBookModel::where('id_book',$id)->delete();
                foreach ($request->get('pakhshselects') as $key => $value) {
                    $pakhsh = PakhshBookModel::create(['id_book'=>$book->id,'id_pakhsh'=>$value]);
                }
            }


            return redirect('admin/books');

        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = NevisanehAndBookModel::where('id_books',$id)->delete();
        $delete2 = BookAndMotarjemModel::where('id_book',$id)->delete();
        $delete3 = PakhshBookModel::where('id_book',$id)->delete();
        $delete4 = NobatChapModel::where('id_books',$id)->delete();
        $delete5 = BooksModel::where('id',$id)->delete();
        return redirect('admin/books');
    }
}

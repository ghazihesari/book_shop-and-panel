<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MotarjemModel;
use App\Http\Requests; 
use App\Http\Requests\MotarjemsRequest;
use Auth;

class MotarjemController extends Controller
{ 

    public function __construct()
    {
        if ( Auth::check() ) {
            $this->middleware('AdminMiddle');
        }else{
            $this->middleware('auth');
        }
    }

    
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $motarjem = MotarjemModel::orderby('id','desc')->paginate(6);
        return View('admin.motarjem.index', ['motarjem'=>$motarjem]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.motarjem.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */ 
    public function store(MotarjemsRequest $request)
    {
        $motarjem = new MotarjemModel( $request->all() );
        if ( $motarjem->save() ) {
            return redirect('admin/motarjems');
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = MotarjemModel::find($id);
        return View( 'admin.motarjem.edit' , ['record'=>$record] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MotarjemsRequest $request, $id)
    {
        $re = MotarjemModel::find($id);
        if ( $re->update( $request->all() ) ) {
            return redirect('admin/motarjems');
        }
        else
        { 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = MotarjemModel::find( $id )->delete();
        return redirect('admin/motarjems');
    }
}

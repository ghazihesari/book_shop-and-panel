<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubjectsModel;
use App\Http\Requests\CategoryRequests;
use App\Http\Requests;
use Auth;

class CategoryController extends Controller
{

    public function __construct()
    {
        if ( Auth::check() ) {
            $this->middleware('AdminMiddle');
        }else{
            $this->middleware('auth');
        }
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorys = SubjectsModel::orderBy('id','desc')->paginate(12);
        return View('admin.category.index',['categorys'=>$categorys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = ['-'=>'ﻧﺎﻡ ﺩﺳﺘﻪ ﻣﺎﺩﺭ ﺭا اﻧﺘﺨﺎﺏ کﻥیﺩ'] + SubjectsModel::where('replay_subjects','-')->orderBy('id','desc')->lists('name_subjects','id')->toArray();
        return View('admin.category.create',['category'=>$category]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequests $request)
    {
        $category = new SubjectsModel( $request->all() );
        if ( $category->save() ) {
            return redirect('admin/category');
        }
        else{
           return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = ['-'=>'ﻧﺎﻡ ﺩﺳﺘﻪ ﻣﺎﺩﺭ ﺭا اﻧﺘﺨﺎﺏ کﻥیﺩ'] + SubjectsModel::where('replay_subjects','-')->orderBy('id','desc')->lists('name_subjects','id')->toArray();

        $record = SubjectsModel::find($id);

        return View('admin.category.edit',['category'=>$category,'record'=>$record]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequests $request, $id)
    {
        $re = SubjectsModel::find($id);
        if ( $re->update( $request->all() ) ) {
            return redirect('admin/category');
        }
        else
        { 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $get = SubjectsModel::where('replay_subjects',$id)->get();

        foreach ($get as $value) {
            SubjectsModel::where('id',$value->id)->update(['replay_subjects'=>'-']);
        }

        $delete = SubjectsModel::find( $id )->delete();

        return redirect('admin/category');
    }
}

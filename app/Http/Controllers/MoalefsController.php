<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MoalefRequest;
use App\Http\Requests;
use App\NevisandehModel;
use Auth;
 
class MoalefsController extends Controller
{

    public function __construct()
    {
        if ( Auth::check() ) {
            $this->middleware('AdminMiddle');
        }else{
            $this->middleware('auth');
        }
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nevisandehs = NevisandehModel::orderby('id','desc')->paginate(6);
        return View('admin.moalef.index', ['nevisandehs'=>$nevisandehs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.moalef.create');
    }
 
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MoalefRequest $request)
    {
        $moalef = new NevisandehModel( $request->all() );
        if ( $moalef->save() ) {
            return redirect('admin/moalef');
        }
        else{
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $record = NevisandehModel::find($id);
        return View( 'admin.moalef.edit' , ['record'=>$record] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $re = NevisandehModel::find($id);
        if ( $re->update( $request->all() ) ) {
            return redirect('admin/moalef');
        }
        else
        { 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = NevisandehModel::find( $id )->delete();
        return redirect('admin/moalef');
    }
}

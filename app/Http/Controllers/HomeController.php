<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\SubjectsModel;
use App\BooksModel;
use Session;
use App\NevisanehAndBookModel;
use App\BookAndMotarjemModel;
use App\PakhshBookModel;
use App\NobatChapModel;
use App\CommentModel;
use App\SefareshModel;
use App\ContentSefareshModel;
use App\lib\Bitpay;
use Auth;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = SubjectsModel::where('replay_subjects','-')->orderby('id','desc')->get();
        $newcontent = BooksModel::orderby('id','desc')->take(8)->get();
        $newtop = BooksModel::orderby('view_book','desc')->take(4)->get();
        return View('index.index',['category'=>$category,'newcontent'=>$newcontent,'newtop'=>$newtop]);
    }


    public function category( $category )
    {
        $id = SubjectsModel::where('name_subjects',$category)->first()['id'];
        $category = SubjectsModel::where('replay_subjects','-')->orderby('id','desc')->get();
        $books = BooksModel::where('id_subjects',$id)->orderby('id','desc')->paginate(12);
        return View('index.shop',['category'=>$category,'books'=>$books]);
    }


    public function single( $url )
    { 

        $category = SubjectsModel::where('replay_subjects','-')->orderby('id','desc')->get();
        $record = BooksModel::where('url_book',$url)->orderby('id','desc')->first();

        $moalefs = NevisanehAndBookModel::where('id_books',$record->id)->get();

        $motarjems = BookAndMotarjemModel::where('id_book',$record->id)->get();

        $pakhshs = PakhshBookModel::where('id_book',$record->id)->get();

        $countprint = NobatChapModel::where('id_books',$record->id)->orderby('id','desc')->first();

        $comments = CommentModel::where(['id_books'=>$record->id,'replaye_comments'=>'-','state'=>1])->get();

        $record->view_book += 1;

        if ( $record->update() ) {
            
            return View('index.single',['category'=>$category,'record'=>$record,'moalefs'=>$moalefs,'motarjems'=>$motarjems,'pakhshs'=>$pakhshs,'countprint'=>$countprint,'comments'=>$comments]);
         } 
    }

    public function add(Request $request)
    {
        if ( session::has('cart') ) {
            $cart = session::get('cart');
            if ( array_key_exists( $request->id_book , $cart ) ) {
                $cart[$request->id_book]++;
            }else{
                $cart[$request->id_book]=1;
            }
            session::put('cart',$cart);
        }else{
            $cart = array();
            $cart[$request->id_book]=1;
            session::put('cart',$cart);
        }

        return View('index.cart');
    }

    public function delete( Request $request )
    {
        $cart=session::get('cart');
        Session::forget('cart');
        $cart2=array();
        foreach ($cart as $key => $value) {        
            if ($key!=$request->id_book) {
                $cart2[$key]=$value;  
            }
            else
            {
                $count=$cart[$request->id_book] - 1;
                if($count==0)
                {
                }
                else
                {
                    $cart2[$request->id_book]=$count;
                }
            }

        }
        session::put('cart',$cart2);
        return View('index.cart');
    }

    public function empty_cart()
    {
        Session::forget('cart');
        return View('index.cart');
    }


    public function shop()
    {
        $category = SubjectsModel::where('replay_subjects','-')->orderby('id','desc')->get();
        $books = BooksModel::orderby('id','desc')->paginate(12);
        return View('index.shop',['category'=>$category,'books'=>$books]);
    }

    public function comment( Request $request )
    {
        $comment = new CommentModel( $request->all() );
        $comment->replaye_comments = '-';
        $comment->state = '0';

        $url = BooksModel::where('id',$request->id_books)->first()['url_book'];
        if ( $comment->save() ) {
            return redirect( '/book/'.$url );
        }else{
            return redirect( '/book/'.$url );
        }
    }
 

    public function checkout()
    {
        return View('index.checkout');
    }
    public function checkoutstep2()
    {
        return View('index.checkout-step2');
    }
    public function checkoutstep3()
    {
        return View('index.checkout-step3');
    }

    public function savecheckout( Request $request )
    {
        $sef = new SefareshModel( $request->all() );
        $sef->codepaygiry_sefareshats = 'pay'.time();
        $sef->codesefsresh_sefareshats = time();
        $sef->state = 0;
        if ( $sef->save() ) {
            return View('index.checkout-step3',['sef'=>$sef]);
        }
    }

 
    public function success( $id )
    {

        $price = 0;
        foreach ( Session::get('cart') as $key=>$value )
        {
            $content = ContentSefareshModel::create(['id_sefareshats'=>$id,'id_books'=>$key,'count'=>$value]);

            $price+= BooksModel::where('id',$key)->first()['price_book']* $value;
        }
        
        Session::forget('cart');

        $sefaresh = SefareshModel::find($id);
        $sefaresh->state = 0;
        $sefaresh->price = $price;
        if ( $sefaresh->update() ) {

            $url = 'http://bitpay.ir/payment-test/gateway-send'; 
            $api = 'adxcv-zzadq-polkjsad-opp13opoz-1sdf455aadzmck1244567';
            $amount = $price;
            $redirect = 'http://localhost:8000/buyback';
            $name = $sefaresh->name_sefareshats;
            $email = $sefaresh->email_sefareshats;
            $description = $sefaresh->address_sefareshats;
            $factorId = $id;

            $result = Bitpay::send($url,$api,$amount,$redirect,$factorId,$name,$email,$description);
            
            if( $result > 0 && is_numeric($result) )
            {
                Session::put('id_sefaresh',$id);
                return redirect('http://bitpay.ir/payment-test/gateway-'.$result);


            }else if ( $result == -1 ) {
                # code...
            }else{
                var_dump( $result );
            }


        }
    }


    public function search( Request $request )
    {
        $category = SubjectsModel::where('replay_subjects','-')->orderby('id','desc')->get();
        $books = BooksModel::where('name_book','LIKE','%'.$request->search.'%')->orderby('id','desc')->paginate(12);
        return View('index.shop',['category'=>$category,'books'=>$books]);
    }


    public function buy()
    {
        $url = 'http://bitpay.ir/payment-test/gateway-send'; 
        $api = 'adxcv-zzadq-polkjsad-opp13opoz-1sdf455aadzmck1244567';
        $amount = 1000;
        $redirect = 'http://localhost:8000/buyback';
        $name = 'testname';
        $email = 'test@yahoo.com';
        $description = 'test content';
        $factorId = 1;

        $result = Bitpay::send($url,$api,$amount,$redirect,$factorId,$name,$email,$description);
        
        if( $result > 0 && is_numeric($result) )
        {

            return redirect('http://bitpay.ir/payment-test/gateway-'.$result);


        }else if ( $result == -1 ) {
            # code...
        }else{
            var_dump( $result );
        }


    }

    public function buypost( Request $request )
    {
        $url = 'http://bitpay.ir/payment-test/gateway-result-second'; 
        $api = 'adxcv-zzadq-polkjsad-opp13opoz-1sdf455aadzmck1244567';
        $trans_id = $request->trans_id; 
        $id_get = $request->id_get;

        $sefaresh = SefareshModel::find(Session::get('id_sefaresh'));
        $sefaresh->id_get = $id_get;
        $sefaresh->trans_id = $trans_id;

        $result = Bitpay::get($url,$api,$trans_id,$id_get); 

        if ( $result == 1 ) {
            $sefaresh->state = 1;
            if ( $sefaresh->update() ) {
                Session::forget('id_sefaresh');
                return redirect('/');
            }
        }


    }

}

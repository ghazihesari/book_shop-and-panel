<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/category/{category}','HomeController@category');
Route::get('/book/{url}','HomeController@single');
Route::post('/add','HomeController@add');
Route::post('/remove','HomeController@delete');
Route::post('/empty','HomeController@empty_cart');
Route::get('/shop','HomeController@shop');
Route::post('/comment','HomeController@comment');
Route::post('/search','HomeController@search'); 

Route::get('/buy','HomeController@buy');
Route::post('/buyback','HomeController@buypost');
 
Route::get('/checkout','HomeController@checkout');
Route::get('/checkout-step2','HomeController@checkoutstep2');
Route::post('/checkout','HomeController@savecheckout');
Route::get('/checkout-step3','HomeController@checkoutstep3');
Route::get('/success/sefaresh/{id}','HomeController@success');
 
Route::auth(); 

Route::group( ['prefix'=>'admin'] , function()
{
	Route::resource('/pakhsh','PakhshController');
	Route::resource('/ticket','TicketController');
	Route::post('/ticket/ansewer','TicketController@ansewer');
	Route::get('/ticket/ansewer/{id}','TicketController@ansewerget');
	Route::resource('/sefareshs','SefareshsController');
	Route::get('/state/sefaresh/{idsefaresh}/{idstate}','SefareshsController@editstate');
	Route::resource('/books','BooksController');
	Route::resource('/users', 'UserController');
	Route::resource('/moalef','MoalefsController');
	Route::resource('/motarjems','MotarjemController');
	Route::resource('/chapkhoneh','ChapKhonehController');
	Route::resource('/category','CategoryController');
	Route::resource('/comments','CommentController');
	Route::get('/comments/success/{id}','CommentController@success');
	Route::get( 'index' ,[ 'middleware'=>'AdminMiddle', function()
	{
		return View('admin.index');
	}]);

});


Route::group(['prefix'=>'user'], function()
{
	Route::get('/panel','UserPanelController@index');
	Route::resource('/ticket','UserPanelTicketController');
});


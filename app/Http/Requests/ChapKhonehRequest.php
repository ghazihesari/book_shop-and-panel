<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ChapKhonehRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_chapkhonehs' => 'required',
            'year_chapkhonehs' => 'required',
            'phone_chapkhonehs' => 'required',
            'email_chapkhonehs' => 'required',
            'website_chapkhonehs' => 'required',
            'address_chapkhonehs' => 'required',
            'details_chapkhonehs' => 'required'
        ];
    }

    public function attributes()
    {
        return[
            'name_chapkhonehs' => 'نام چاپ خانه',
            'year_chapkhonehs' => 'سال تاسیس چاپ خانه',
            'phone_chapkhonehs' => 'تلفن چاپ خانه',
            'email_chapkhonehs' => 'ایمیل چاپ خانه',
            'website_chapkhonehs' => 'وب سایت چاپ خانه',
            'address_chapkhonehs' => 'آدرس چاپ خانه',
            'details_chapkhonehs' => 'توضیحات چاپ خانه'
        ];
    }

    public function messages()
    {
        return[
            'required' => ':attribute به درستی وارد نشده است.'
        ];
    }
}

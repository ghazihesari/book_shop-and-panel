<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MotarjemsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_motarjems' => 'required',
            'lname_motarjems' => 'required',
            'age_motarjems' => 'required',
            'maghtah_motarjems' => 'required',
            'reshtah_motarjems' => 'required',
            'phone_motarjems' => 'required',
            'details_motarjems' => 'required',
            'email_motarjems' => 'required',
            'website_motarjems' => 'required'
        ];
    }

    public function attributes()
    {
        return[
            'name_motarjems' => 'نام مترجم',
            'lname_motarjems' => 'نام خانوادگی مترجم',
            'age_motarjems' => 'سم مترجم',
            'maghtah_motarjems' => 'مقطع تحصیلی',
            'reshtah_motarjems' => 'رشته تحصیلی',
            'phone_motarjems' => 'تلفن مترجم',
            'details_motarjems' => 'توضیحات اضافی مترجم',
            'email_motarjems' => 'ایمیل مترجم',
            'website_motarjems' => 'وب سایت مترجم'
        ];
    }

    public function messages()
    {
        return[
            'required' => ':attribute نباید خالی وارد شود.'
        ];
    }
}

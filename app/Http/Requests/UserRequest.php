<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'fname' => 'required',
            'lname' => 'required',
            'role' => 'required',
            'imguser' => 'required|max:300|mimes:jpg,jpeg,png'
        ];
    }

    public function attributes()
    {
        return[
            'name' => 'نام کاربری',
            'email' => 'ایمیل ',
            'password' => 'گذرواژه',
            'fname' => 'نام ',
            'lname' => 'نام خانوادگی',
            'role' => 'نقش',
            'imguser' => 'تصویر'
        ];
    }

    public function messages()
    {
        return[
            'required' => ':attribute به درستی وارد نشده است.',
            'max' => ':attribute نباید بیشتر از 300 کیلو بایت باشد.',
            'mimes' => ':attribute فقط نوع فایل های jpeg-jpg-png می تواند باشد.'
        ];
    }
}

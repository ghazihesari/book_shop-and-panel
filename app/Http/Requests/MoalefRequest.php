<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MoalefRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_moalefs' => 'required',
            'lname_moalefs' => 'required',
            'details_moalefs' => 'required',
            'website_moalefs' => 'required',
            'phone_moalefs' => 'required',
            'address_moalefs' => 'required'
        ];
    }

    public function attributes()
    {
        return[
            'name_moalefs' => 'نام نویسنده',
            'lname_moalefs' => 'نام خانوادگی نویسنده',
            'details_moalefs' => 'توضیحات نویسنده',
            'website_moalefs' => 'وب سایت رسمی نویسنده',
            'phone_moalefs' => 'تلفن نویسنده',
            'address_moalefs' => 'آدرس نویسنده'
        ];
    }

    public function messages()
    {
        return[
            'required' => ':attribute به درستی وارد نشده است.'
        ];
    }
}
